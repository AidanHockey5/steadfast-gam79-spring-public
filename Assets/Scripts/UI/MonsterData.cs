﻿using UnityEngine;
using System.Collections;

public class MonsterData 
{
	public int health;
	public int maxHealth;
	public bool hasFire;
	public bool hasCorrosive;
	public bool hasElectric;
	public bool hasSludge;
}
