﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NuclearPowerPlantParticles : NetworkBehaviour
{
    ParticleManager pm;
    public Building building;

    public int particleLimit = 0;

    public bool isHit = false;
    [SerializeField]
    private BoxCollider buildingCollider = null;

    private Vector3 minBound;
    private Vector3 maxBound;
	// Use this for initialization
	void Start () 
    {
        building = GetComponent<Building>();
        pm = ParticleManager.Instance;

        buildingCollider = GetComponent<BoxCollider>();

        if (GetComponent<BoxCollider>() != null)
        {
            minBound = buildingCollider.bounds.min;
            maxBound = buildingCollider.bounds.max;
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (this.building.currentHealth < this.building.health && !isHit)
        {
            isHit = true;
            if (isHit == true)
            {
                SpawnRandomFire();
            }
           
        }
        if (this.building.health <= 0)
        {
            FinalErutption();
        }
	}
    private void SpawnRandomFire()
    {
        if (pm != null && buildingCollider != null)
        {
            float randomSide = Random.value;
            int minOrMax = Random.Range(0, 2);

            float randomX = Random.Range(minBound.x, maxBound.x);
            float randomY = Random.Range(minBound.y, maxBound.y);
            float randomZ = Random.Range(minBound.z, maxBound.z);

            Vector3 spawnPoint = new Vector3();

            if (randomSide > 0.6f)
            {
                if (minOrMax == 0)
                    spawnPoint = new Vector3(maxBound.x, randomY, randomZ);
                else
                    spawnPoint = new Vector3(minBound.x, randomY, randomZ);
            }
            else
                if (randomSide > 0.2)
                {
                    if (minOrMax == 0)
                        spawnPoint = new Vector3(randomX, randomY, maxBound.z);
                    else
                        spawnPoint = new Vector3(randomX, randomY, minBound.z);
                }
                else
                {
                    spawnPoint = new Vector3(randomX, maxBound.y, randomZ);
                }

            Vector3 direction = transform.position - spawnPoint;
            //Quaternion rotation = Quaternion.FromToRotation(-transform.forward, direction);


            spawnPoint += (direction.normalized * particleLimit);

            // ParticleSystem fireSpawn = (ParticleSystem)Instantiate(fireParticle, spawnPoint, rotation);
            if (this.building.currentHealth < this.building.health)
            {
                pm.RpcSpawnParticle("Bldg Hit", spawnPoint, 1f);
                pm.RpcSpawnParticle("Dust_debree", spawnPoint, 0);
            }
            if (this.building.health < 1500)
            {
                pm.RpcSpawnParticle("explosion_Small", spawnPoint, 10f);
                pm.RpcSpawnParticle("FireComplex", spawnPoint, 0);
                pm.RpcSpawnParticle("NEW_flame&blacksmoke", spawnPoint, 0);
            }
            if (this.building.health < 1000)
            {
                pm.RpcSpawnParticle("Explosion", spawnPoint, 25f);
                pm.RpcSpawnParticle("NukeSteam", spawnPoint, 0);
            }
           //transform.parent = gameObject.GetComponent<BoxCollider>().transform;

        }
    }
   
    public void FinalErutption()
    {
        pm.RpcSpawnParticle("NEW_NuclearPlantExplosion", this.transform.position, 45f);
        pm.RpcSpawnParticle("RadiationCloud", this.transform.position, 0);
        AfterMath();
    }
    public void AfterMath()
    {
        pm.RpcSpawnParticle("NukeSteam", transform.position, 0);
    }
}
