﻿using System;

public class GameEventArgs : EventArgs
{
	public GameEvent eventType { get; set; }

	public float amount { get; set; }

	public bool condition { get; set; }

	public object[] eventArgs { get; set; }
}
