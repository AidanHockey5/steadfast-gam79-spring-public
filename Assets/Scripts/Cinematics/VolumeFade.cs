﻿using UnityEngine;
using System.Collections;

public class VolumeFade : MonoBehaviour 
{
    AudioSource aud;
	// Use this for initialization
	void Start () 
    {
        aud = GetComponent<AudioSource>();
        aud.volume = 0;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (aud.volume != 1)
            aud.volume = Mathf.MoveTowards(aud.volume, 1f, Time.deltaTime);
	}
}
