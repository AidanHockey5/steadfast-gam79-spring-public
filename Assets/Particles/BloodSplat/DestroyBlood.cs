﻿using UnityEngine;
using System.Collections;

public class DestroyBlood : MonoBehaviour
{
    public float timer;
	// Use this for initialization
	void Start () 
    {
        timer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () 
    {
        timer = timer + Time.deltaTime;
        if(timer >= .5f)
        {
            Destroy(gameObject);
        }
	}
}
