﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LobbyAFK : MonoBehaviour 
{
    Image img;
    public float transitionAfterSeconds = 30f;
    public float transitionSpeed = 2f;
    public string sceneToLoad = null;
    float timer = 0f;
    bool openNextScene = false;
    public bool fade = false;
    public bool lockFade = false;
    GameObject[] audioSources;
	// Use this for initialization
	void Awake () 
    {
        img = GetComponent<Image>();
        img.color = Color.black;
	}

    void Start()
    {
        audioSources = GameObject.FindGameObjectsWithTag("LocalSound");
    }
	
	// Update is called once per frame
	void Update () 
    {
        timer += Time.deltaTime;
        if(Input.anyKey || (Mathf.Abs(Input.GetAxis("Mouse X"))+Mathf.Abs(Input.GetAxis("Mouse Y")))>0) //If there is any input detected, the user is not AFK
            timer = 0f;
        if(timer >= transitionAfterSeconds && !lockFade)
        {
            fade = true;
            openNextScene = true;
        }
        if (openNextScene)
        {
			LocalAudioManager.instance.LocalSound ("musicFade");
			/*foreach (GameObject g in audioSources)
            {
                AudioSource aud = g.GetComponent<AudioSource>();
                aud.volume = Mathf.MoveTowards(aud.volume, 0f, Time.deltaTime * 3f);
            }*/
        }
        ControlFade();
	}

    public void fadeLock()
    {
        lockFade = true;
    }

    public void fadeUnLock()
    {
        lockFade = false;
    }

    void ControlFade()
    {
        if(fade)
        {
            if(img.color.a != 1)
                img.color = new Color(img.color.r, img.color.g, img.color.b, Mathf.MoveTowards(img.color.a, 1f, Time.deltaTime * transitionSpeed));
            else
                fade = false;
            if (img.color.a == 1f && openNextScene) //Load new scene once transition has completed
            {
                SceneManager.LoadScene(sceneToLoad);
            }
        }
        else
        {
            if (img.color.a != 0)
                img.color = new Color(img.color.r, img.color.g, img.color.b, Mathf.MoveTowards(img.color.a, 0f, Time.deltaTime * transitionSpeed));
        }
    }
}
