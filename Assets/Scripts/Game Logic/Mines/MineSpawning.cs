﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public class MineSpawning : NetworkBehaviour, IEventListener
{
    public GameObject minePrefab;
    public Transform spawnPoint;
    public float spawnRate = 1.0f;

    private int mineCounter = 0;
    private bool canSpawn = true;

    #region Monobehaviours

    [ClientCallback]
    void Start()
    {
		mineCounter = 3;
        if (NetworkClient.active)
        {
            ClientScene.RegisterPrefab(minePrefab);
            Subscribe();
        }
    }

    void OnDestroy()
    {
        UnSubscribe();
    }

    #endregion

    [Command]
    private void CmdSpawnMine()
    {
        if (mineCounter > 0 && canSpawn == true)
        {
			GameObject mine = (GameObject) Instantiate(minePrefab, spawnPoint.position, Quaternion.identity);
			NetworkServer.Spawn(mine);
            mineCounter--;
            canSpawn = false;
            StartCoroutine(StartSpawnTimer(spawnRate));
        }       
    }

    private IEnumerator StartSpawnTimer(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        canSpawn = true;
    }

    #region IEventListener

    public void Subscribe()
    {
		InputManager.Instance.RegisterHandler (ReceiveBroadcast);
    }

    public void UnSubscribe()
    {
		InputManager.Instance.UnRegisterHandler (ReceiveBroadcast);
	}

    public void ReceiveBroadcast(object sender, GameEventArgs gameEventArgs)
    {
        switch (gameEventArgs.eventType)
        {
            case GameEvent.CHARACTER_FIRE2:
            {
                    // gameEventArgs.eventArgs[0] - bool fire2Input
                if (gameEventArgs.condition)
                {
                    CmdSpawnMine();
                }        
            }
            break;
        }
    }

    #endregion

}
