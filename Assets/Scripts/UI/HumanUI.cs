﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HumanUI : UIController 
{
	[SerializeField]
	private Image _healthBar;
	[SerializeField]
	private GameObject _shieldOne;
	[SerializeField]
	private GameObject _shieldTwo;
	[SerializeField]
	private GameObject _shieldThree;
	[SerializeField]
	private GameObject _shieldFour;
	[SerializeField]
	private GameObject _shieldFive;
	[SerializeField]
	private Text _ammoText;
	[SerializeField]
	private Image _abilityOneCooldownImage;
	[SerializeField]
	private Image _abilityTwoCooldownImage;
	[SerializeField]
	private Image _reloadImage;

	private int _maxHealth = 1;
	private int _health = 0;
	private int _maxShield = 1;
	private int _shield = 0;
	private int _maxClip = 1;
	private int _clip = 1;
	private float _abilityOneCooldown = 1.0f;
	private float _abilityOneCount = 1.0f;
	private float _abilityTwoCooldown = 1.0f;
	private float _abilityTwoCount = 1.0f;
	private bool _abilityOneOnCooldown = false;
	private bool _abilityTwoOnCooldown = false;
	private float _reloadTime = 1.0f;
	private float _reloadCount = 1.0f;
	private bool _reloading = false;
	private bool _updateCooldowns;

	void Update()
	{
		_updateCooldowns = false;

		if (_abilityOneOnCooldown && _abilityOneCooldownImage != null)
		{			
			_abilityOneCount -= Time.deltaTime;
			if (_abilityOneCount <= 0)
			{
				_abilityOneCount = _abilityOneCooldown;
				_abilityOneOnCooldown = false;
				_abilityOneCooldownImage.gameObject.SetActive (false);
			}
			_updateCooldowns = true;
		}

		if (_abilityTwoOnCooldown && _abilityTwoCooldownImage != null)
		{
			_abilityTwoCount -= Time.deltaTime;
			if (_abilityTwoCount <= 0)
			{
				_abilityTwoCount = _abilityOneCooldown;
				_abilityTwoOnCooldown = false;
				_abilityTwoCooldownImage.gameObject.SetActive (false);
			}
			_updateCooldowns = true;
		}

		if (_reloading && _reloadImage != null)
		{
			_reloadCount -= Time.deltaTime;
			if (_reloadCount <= 0)
			{
				_reloadCount = _reloadTime;
				_reloading = false;
				_reloadImage.gameObject.SetActive (false);
			}
			_updateCooldowns = true;
		}

		if(_updateCooldowns)
			UpdateCooldowns();
	}

	protected override void OnDataChanged ()
	{
		base.OnDataChanged ();
		if (_playerData.maxHealth != _maxHealth)
			_maxHealth = _playerData.maxHealth;
		if (_playerData.health != _health)
			OnHealthChanged ();
		if (_playerData.maxShield != _maxShield)
			_maxShield = _playerData.maxShield;
		if (_playerData.shield != _shield)
			OnShieldChanged ();
		if (_playerData.maxClip != _maxClip)
			_maxClip = _playerData.maxClip;
		if (_playerData.clip != _clip)
			OnClipChanged ();
		if (_playerData.abilityOneCooldown != _abilityOneCooldown)
		{
			_abilityOneCooldown = _playerData.abilityOneCooldown;
			_abilityOneCount = _abilityOneCooldown;
		}
		if (_playerData.abilityTwoCooldown != _abilityTwoCooldown)
		{
			_abilityTwoCooldown = _playerData.abilityTwoCooldown;
			_abilityTwoCount = _abilityTwoCooldown;
		}
		if (_playerData.reloadTime != _reloadTime)
		{
			_reloadTime = _playerData.reloadTime;
			_reloadCount = _playerData.reloadTime;
		}
	}

	private void OnHealthChanged()
	{
		_health = _playerData.health;
		float currentHealth = (float)_health / (float)_maxHealth;
		_healthBar.rectTransform.localScale = new Vector3 (currentHealth, _healthBar.transform.localScale.y, _healthBar.transform.localScale.z);
	}

	private void OnShieldChanged()
	{
		_shield = _playerData.shield;
		float currentShield = (float)_shield / (float)_maxShield;

		_shieldOne.SetActive (true);
		_shieldTwo.SetActive (true);
		_shieldThree.SetActive (true);
		_shieldFour.SetActive (true);
		_shieldFive.SetActive (true);

		if (currentShield <= 0.8f)
			_shieldOne.SetActive (false);
		if (currentShield <= 0.6f)
			_shieldTwo.SetActive (false);
		if (currentShield <= 0.4f)
			_shieldThree.SetActive (false);
		if (currentShield <= 0.2f)
			_shieldFour.SetActive (false);
		if (currentShield <= 0.0f)
			_shieldFive.SetActive (false);
	}

	private void OnClipChanged()
	{
		_clip = _playerData.clip;
		_ammoText.text = string.Format ("{0} / {1}", _clip, _maxClip);
	}

	private void UpdateCooldowns()
	{
		if(_abilityOneCooldownImage != null)
			_abilityOneCooldownImage.fillAmount = _abilityOneCount / _abilityOneCooldown;
		if(_abilityTwoCooldownImage != null)	
			_abilityTwoCooldownImage.fillAmount = _abilityTwoCount / _abilityTwoCooldown;
		if (_reloadImage != null)
			_reloadImage.fillAmount = _reloadCount / _reloadTime;
	}

	protected override void OnTriggerCooldown (int index)
	{
		base.OnTriggerCooldown (index);
		TriggerCooldown (index);
	}

	public void TriggerCooldown(int cooldown)
	{
		switch (cooldown) 
		{
		case 1:
			_abilityOneOnCooldown = true;
			if(_abilityOneCooldownImage != null)
				_abilityOneCooldownImage.gameObject.SetActive (true);
			break;
		case 2:
			_abilityTwoOnCooldown = true;
			if(_abilityTwoCooldownImage != null)
				_abilityTwoCooldownImage.gameObject.SetActive (true);
			break;
		case 3:
			_reloading = true;
			if (_reloadImage != null)
				_reloadImage.gameObject.SetActive (true);
			break;
		default:
			Debug.LogError ("Cooldown index not present");
			break;
		}
	}
}
