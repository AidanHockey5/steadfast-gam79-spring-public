﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingScale : MonoBehaviour 
{
    public float floors = 3;
    public GameObject floor2 = null;
    public GameObject floor3 = null;
    public float floorHeight = 4.0001f;

    public List<Rigidbody> gravObjs = new List<Rigidbody>();
    private float delayTime = 0.5f;
    public bool destroyPieces = false;
	public List<ParticleSystem> particles = new List<ParticleSystem>();

    public DestroyCube dc;

    public void Start()
    {
        StartCoroutine(BuildingCollapse());
    }

    public IEnumerator BuildingCollapse()
    {
        yield return new WaitForSeconds(delayTime);

		foreach (ParticleSystem ps in particles) 
		{
			ps.Play ();
		}

        foreach (Rigidbody gObj in gravObjs)
        {
            gObj.useGravity = true;
			gObj.constraints = RigidbodyConstraints.None;

            if(destroyPieces)
                dc.canDestroy = true;
        }

        StartCoroutine(StopParticles());
    }

    public IEnumerator StopParticles()
    {
        yield return new WaitForSeconds(3f);

		foreach (ParticleSystem ps in particles) 
		{
			ps.Stop ();
		}
    }
}
