﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using UnityEngine.Audio;

public class PlayerObject : NetworkBehaviour, IEventBroadcaster, IEventListener
{
	
#region Public Fields

	public bool healthWarnPlay = false;
	public bool critWarnPlay = false;

	[Header ("Sync Variables")]
	[SyncVar (hook = "OnChangeHealth")]
	public int syncCurrentHealth = 0;
	[SyncVar]
	public bool syncIsAlive = false;

	[Header ("Settings (Initialization Only)")]
	public InputSettings inputSettings;
	public PlayerSettings playerSettings;
	public CameraSettings cameraSettings;
	public MovementSettings movementSettings;
	public AnimatorSettings animatorSettings;
	public MonsterAbilitySettings[] monsterAbilitySettings;
	public WeaponSettings[] equippableWeapons;

	public event EventHandler<GameEventArgs> EventHandlerMainDelegate;

#endregion

#region Private Members

	private CameraController camController = null;
	private GameEventArgs buffer = new GameEventArgs ();
	private PlayerData playerData = new PlayerData ();
	private MonsterData monsterData = new MonsterData ();

#endregion

#region MonoBehaviours

	[Command]
	public void CmdInitialize(int prefab)
	{
		Vector3 spawnPoint = SpawnPointManager.Instance.SpawnPointLocation (1);

		if (prefab == 1)
			spawnPoint = CustomNetworkManager.Instance.startPositions [0].position;
		
		GameObject obj = (GameObject)Instantiate (CustomNetworkManager.Instance.spawnPrefabs [prefab], spawnPoint, Quaternion.identity);
		NetworkServer.Destroy (this.gameObject);
		NetworkServer.ReplacePlayerForConnection (this.connectionToClient, obj, this.playerControllerId);
	}

	private void Start ()
	{
		if (CustomNetworkManager.Instance.Initialized == false)
		{
			if (!isLocalPlayer)
				return;
			
			if (CustomNetworkManager.Instance.IsHost)
			{
				if (CustomNetworkManager.Instance.IsHuman)
					CmdInitialize (0);
				else
					CmdInitialize (1);
			}
			else
				CmdInitialize (0);
			CustomNetworkManager.Instance.Initialized = true;
		}
		else
		{
			camController = GetComponent<CameraController> ();
			syncIsAlive = true;
			switch (playerSettings.playerType)
			{
			case PlayerType.MONSTER:
				{	
					monsterData.health = playerSettings.maxHealth;
					monsterData.maxHealth = playerSettings.maxHealth;
					gameObject.transform.Rotate (0.0f, -90.0f, 0.0f);
				}
				break;
			case PlayerType.HUMAN:
			case PlayerType.MECH:
				{
					playerData.health = playerSettings.maxHealth;
					playerData.maxHealth = playerSettings.maxHealth;
					CharacterMovementController cont = GetComponent<CharacterMovementController> ();

					if (cont != null)
					{
						if (cont.equippedSpecial == SpecialMovementState.HAS_GRAPPLE_HOOK)
							playerData.abilityOneCooldown = 1.0f;
						else if (cont.equippedSpecial == SpecialMovementState.HAS_JETPACK)
							playerData.abilityOneCooldown = 5.0f;
					}

					playerData.maxClip = equippableWeapons [0].maxAmmo;
					playerData.clip = equippableWeapons [0].maxAmmo;
					playerData.reloadTime = equippableWeapons [0].reloadTime;
				}
				break;
			}

			if (isLocalPlayer)
			{
				syncCurrentHealth = playerSettings.maxHealth;
				Subscribe ();
			}
		}

		AudioManager.instance.SetPlayerType (playerSettings.playerType.ToString ());

		if (playerSettings.playerType == PlayerType.HUMAN) 
		{
			if (!isServer)
				AudioManager.instance.PepTalk ();
		}

		if (playerSettings.playerType == PlayerType.MECH) 
		{
			if (!isServer)
				AudioManager.instance.PepTalk ();
		}
	}

	private void OnDestroy ()
	{
		if (isLocalPlayer)
		{
			EventHandlerMainDelegate = null;
			UnSubscribe ();
		}
	}

	public void AudioEvent (string audioEvent)
	{
		if(isServer)
			AudioManager.instance.RpcSound (audioEvent, gameObject);
	}

#endregion

#region Functions

	public void UseAmmo()
	{
		playerData.clip--;
		if(isLocalPlayer)
			UpdateUI ();
	}

	public void Reload()
	{
		if(isServer)
			AudioManager.instance.RpcSound ("reload", gameObject);
		playerData.clip = playerData.maxClip;
		if(isLocalPlayer)
			UpdateUI ();
	}

	public void RequestTakeDamage (GameEvent attacker, int amount)
	{
		switch (attacker)
		{
			case GameEvent.HIT_FROM_HUMAN: 
				{
					if (playerSettings.playerType == PlayerType.MONSTER)
					{
						TakeDamage (amount);
					}
				}
				break;
			case GameEvent.HIT_FROM_MECH:
				{
					if (playerSettings.playerType == PlayerType.MONSTER)
					{
						TakeDamage (amount);
					}
				}
				break;
			case GameEvent.HIT_FROM_MONSTER:
				{
					if (playerSettings.playerType == PlayerType.HUMAN || playerSettings.playerType == PlayerType.MECH)
					{
						TakeDamage (amount);
					}
				}
				break;
		}
	}

	private void UpdateUI ()
	{
		switch (playerSettings.playerType)
		{
			case PlayerType.MONSTER:
				{
					monsterData.health = syncCurrentHealth;
					UIManager.Instance.UpdateMonsterData (monsterData);
				}
				break;
			case PlayerType.HUMAN:
				{
					playerData.health = syncCurrentHealth;
					UIManager.Instance.UpdatePlayerData (playerData);
				}
				break;
			case PlayerType.MECH:
				{
					playerData.health = syncCurrentHealth;
					UIManager.Instance.UpdatePlayerData (playerData);
				}
				break;
		}
	}

	private void OnDeath ()
	{
		if(isServer)
			AudioManager.instance.RpcSound ("death", gameObject);

		//Restors audio mixers to default in prep for next spawn
		switch(playerSettings.playerType)
		{
			case PlayerType.HUMAN:
				{
					if(isServer)
						AudioManager.instance.RpcSound ("humanGoodHealth", gameObject);
				}
			break;
			case PlayerType.MONSTER:
				{
					if(isServer)
						AudioManager.instance.RpcSound ("monGoodHealth", gameObject);
				}
			break;
		}

		if (playerSettings.playerType == PlayerType.HUMAN || playerSettings.playerType == PlayerType.MECH)
		{
			if (GameManager.Instance.TicketsDepleted ())
			{
				GameManager.Instance.CmdGameOver (GameOverState.TicketsDepleted);
				UIManager.Instance.SetUIState (UIState.HumansLose);
			}
			else
			{
				GameManager.Instance.OnPlayerDeath ();
			}
		}
		else
		{
			GameManager.Instance.CmdGameOver (GameOverState.MonsterDeath);
		}
	}

	//FOR DEBUGGING: Damages player on keypress
	/*void Update ()
	{
		if (Input.GetKeyDown (KeyCode.KeypadPlus))
		{
			switch (playerSettings.playerType) 
			{
			case PlayerType.MONSTER:
				{
					syncCurrentHealth += 5000;
					print (syncCurrentHealth + " / " + playerSettings.maxHealth);
				}
				break;
			case PlayerType.HUMAN:
				{
					syncCurrentHealth += 50;
					print (syncCurrentHealth + " / " + playerSettings.maxHealth);
				}
				break;
			case PlayerType.MECH:
				{
					syncCurrentHealth += 500;
					print (syncCurrentHealth + " / " + playerSettings.maxHealth);
				}
				break;
			}
		}

		if (Input.GetKeyDown (KeyCode.KeypadMinus))
		{
			switch (playerSettings.playerType) 
			{
			case PlayerType.MONSTER:
				{
					TakeDamage(5000);
					print (syncCurrentHealth + " / " + playerSettings.maxHealth);
				}
				break;
			case PlayerType.HUMAN:
				{
					TakeDamage(50);
					print (syncCurrentHealth + " / " + playerSettings.maxHealth);
				}
				break;
			case PlayerType.MECH:
				{
					TakeDamage(50);
					print (syncCurrentHealth + " / " + playerSettings.maxHealth);
				}
				break;
			}
		}
	}*/

	private void TakeDamage (int amount)
	{
		if(isServer)
			AudioManager.instance.RpcSound ("damage", gameObject);

		syncCurrentHealth -= amount;

		//FOR PLAYER FEEDBACK AUDIO
		if (syncCurrentHealth > (playerSettings.maxHealth * 0.50)) 
		{
			healthWarnPlay = false;
		}

		if (syncCurrentHealth > (playerSettings.maxHealth * 0.25)) 
		{
			critWarnPlay = false;;
		}

		if (playerSettings.playerType == PlayerType.MECH) 
		{
			if (syncCurrentHealth <= (playerSettings.maxHealth * 0.50) && healthWarnPlay == false) 
			{
				if (isServer) 
				{
					AudioManager.instance.RpcSound2D ("mechLowHealth");
					healthWarnPlay = true;
				}
			}

			if (syncCurrentHealth <= (playerSettings.maxHealth * 0.25) && critWarnPlay == false) 
			{
				if (isServer) 
				{
					AudioManager.instance.RpcSound2D ("mechCritHealth");
					critWarnPlay = true;
				}
			}
		}

		if (playerSettings.playerType == PlayerType.HUMAN) 
		{

			if (syncCurrentHealth <= (playerSettings.maxHealth * 0.50) && healthWarnPlay == false) 
			{
				if(isServer)
					AudioManager.instance.RpcSound ("humanLowHealth", gameObject);
				healthWarnPlay = true;
			}

			if (syncCurrentHealth <= (playerSettings.maxHealth * 0.25) && critWarnPlay == false) 
			{

				if(isServer)
					AudioManager.instance.RpcSound ("humanCritHealth", gameObject);
				critWarnPlay = true;
			}
		}

		if (playerSettings.playerType == PlayerType.MONSTER) 
		{

			if (syncCurrentHealth <= (playerSettings.maxHealth * 0.50) && healthWarnPlay == false) 
			{
				AudioManager.instance.RpcSound ("monLowHealth", gameObject);
				healthWarnPlay = true;
			}

			if (syncCurrentHealth <= (playerSettings.maxHealth * 0.25) && critWarnPlay == false) 
			{
				AudioManager.instance.RpcSound ("monCritHealth", gameObject);
				critWarnPlay = true;
			}
		}
		//END PLAYER FEEDBACK AUDIO SECTION
				
		if (syncCurrentHealth <= 0)
		{
			if (isLocalPlayer)
				OnDeath ();
			syncIsAlive = false;
		}
	}

	private void OnChangeHealth (int health)
	{
		//Removes "low health" sounds is health is restored
		if (isLocalPlayer)
		{
			switch(playerSettings.playerType)
			{
				case PlayerType.HUMAN:
					{
						if (syncCurrentHealth > (playerSettings.maxHealth * 0.30)) 
						{
							if (isServer)
								AudioManager.instance.RpcSound ("humanGoodHealth", gameObject);
						}
					}
				break;
				case PlayerType.MONSTER:
					{
						if (syncCurrentHealth > (playerSettings.maxHealth * 0.30)) 
						{
							if (isServer)
								AudioManager.instance.RpcSound ("monGoodHealth", gameObject);
						}
					}
				break;
			}

			UpdateUI ();
		}
	}

#endregion

#region IEventBroadcaster

	public void RegisterHandler (EventHandler<GameEventArgs> EventHandlerDelegate)
	{
		EventHandlerMainDelegate += EventHandlerDelegate;
	}

	public void UnRegisterHandler (EventHandler<GameEventArgs> EventHandlerDelegate)
	{
		EventHandlerMainDelegate -= EventHandlerDelegate;
	}

	public void BroadcastEvent (GameEvent eventType, float amount)
	{
		buffer.eventType = eventType;
		buffer.amount = amount;

		if (EventHandlerMainDelegate != null)
		{
			EventHandlerMainDelegate (this, buffer);
		}
	}

	public void BroadcastEvent (GameEvent eventType, bool condition)
	{
		buffer.eventType = eventType;
		buffer.condition = condition;

		if (EventHandlerMainDelegate != null)
		{
			EventHandlerMainDelegate (this, buffer);
		}
	}

	public void BroadcastEvent (GameEvent eventType, params object[] args)
	{
		buffer.eventType = eventType;
		buffer.eventArgs = args;

		if (EventHandlerMainDelegate != null)
		{
			EventHandlerMainDelegate (this, buffer);
		}
	}

#endregion

#region IEventListener

	public void Subscribe ()
	{
		if (isLocalPlayer)
		{
			InputManager.Instance.input = inputSettings;
		}
	}

	public void UnSubscribe ()
	{
		if (isLocalPlayer)
		{
			InputManager.Instance.input = null;
		}
	}

	public void ReceiveBroadcast (object sender, GameEventArgs gameEventArgs)
	{
		BroadcastEvent (gameEventArgs.eventType, gameEventArgs);
	}

#endregion
}
