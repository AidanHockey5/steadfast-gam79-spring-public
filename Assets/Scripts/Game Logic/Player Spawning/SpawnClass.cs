﻿public enum SpawnClass
{
	None,
	Monster,
	Technician,
	Support,
	Heavy,
	Assault
}