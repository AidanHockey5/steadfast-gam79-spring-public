﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour 
{
	public RectTransform mainMenuPanel;
	public RectTransform gamesListPanel;
	public RectTransform optionsPanel;
	public InputField directConnectInput;
	public RectTransform playerSelectionPanel;
	public RectTransform loadingPanel;

	private RectTransform _currentPanel;

	private static MainMenu instance_ = null;

	public static MainMenu Instance
	{
		get
		{
			if (instance_ != null)
			{
				return instance_;
			}
			else
			{
				GameObject go = new GameObject();
				return instance_ = go.AddComponent<MainMenu>();
			}
		}
	}

	void Start () 
	{
		instance_ = this;
		_currentPanel = mainMenuPanel;
	}

	public void GoToLoadScreen()
	{
		ChangeTo (loadingPanel);
	}

	public void ChangeTo(RectTransform newPanel)
	{
		if (_currentPanel != null) 
		{
			_currentPanel.gameObject.SetActive (false);
		}
		if (newPanel != null) 
		{
			newPanel.gameObject.SetActive (true);
		}
			
		_currentPanel = newPanel;
	}

	public void OnClickBackButton()
	{
		LocalAudioManager.instance.LocalSound ("click");
		ChangeTo (mainMenuPanel);
	}

	public void OnClickOptions()
	{
		LocalAudioManager.instance.LocalSound ("select");
		ChangeTo (optionsPanel);
	}

	public void OnClickExit()
	{
		LocalAudioManager.instance.LocalSound ("click");
		Application.Quit ();
	}

	public void OnClickHostGame()
	{
		LocalAudioManager.instance.LocalSound ("click");
		//LocalAudioManager.instance.LocalSound ("selectMon");
		LocalAudioManager.instance.LocalSound ("gameStart");
		ChangeTo (loadingPanel);
		CustomNetworkManager.Instance.SetMonser ();
	}

	public void OnClickFindGame()
	{
		LocalAudioManager.instance.LocalSound ("select");
		ChangeTo (gamesListPanel);
		CustomNetworkManager.Instance.StartListening ();
	}

	public void OnClickConnect()
	{
		LocalAudioManager.instance.LocalSound ("click");
		LocalAudioManager.instance.LocalSound ("gameStart");
		CustomNetworkManager.Instance.JoinMatch (directConnectInput.text);
	}

	public void OnClickMonster()
	{
		LocalAudioManager.instance.LocalSound ("click");
		//LocalAudioManager.instance.LocalSound ("selectMon");
		LocalAudioManager.instance.LocalSound ("gameStart");
		ChangeTo (loadingPanel);
		CustomNetworkManager.Instance.SetMonser ();
	}

	public void OnClickHuman()
	{
		LocalAudioManager.instance.LocalSound ("click");
		//LocalAudioManager.instance.LocalSound ("selectHum");
		LocalAudioManager.instance.LocalSound ("gameStart");
		ChangeTo (loadingPanel);
		CustomNetworkManager.Instance.SetSupport ();
	}
}
