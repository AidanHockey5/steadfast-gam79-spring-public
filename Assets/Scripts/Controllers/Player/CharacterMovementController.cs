﻿// Lawrence Lopez

using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent (typeof(NetworkTransform))]
[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(CharacterController))]
[RequireComponent (typeof(PlayerObject))]
[RequireComponent (typeof(CameraController))]
[RequireComponent (typeof(LineRenderer))]
public class CharacterMovementController : NetworkBehaviour, IEventListener
{

#region Public

	[Header ("Special Movement")]
	public SpecialMovementState equippedSpecial = SpecialMovementState.HAS_GRAPPLE_HOOK;
	public float jetPackCooldown = 5.0f;
	public float jetPackHeight = 100.0f;
	public float hookCooldown = 1.0f;
	public float hookDistance = 50.0f;
	public float hookReelSpeed = 50.0f;
	public float minReelDistance = 1.0f;

#endregion

#region Private

	// character movement
	private MovementSettings movementSettings = null;
	private CameraSettings cameraSettings = null;
	private CharacterController characterController = null;
	private CameraController cameraController = null;
	private LineRenderer lineRenderer = null;

	// camera movement
	[SyncVar]
	private float yRotation;
	[SyncVar]
	public bool canSpecialMove = true;
	[SyncVar]
	public bool hooked = false;
	[SyncVar]
	public bool locked = false;
	[SyncVar]
	public bool isMoving = false;
	[SyncVar]
	public bool jumping = false;
	[SyncVar]
	private Vector3 hookPoint = Vector3.zero;
	[SyncVar]
	private Vector3 velocity = Vector3.zero;
	[SyncVar]
	private Vector3 targetDirection = Vector3.zero;

	private bool isControllerRunning = false;
	private float hInput, vInput = 0;
	private Vector3 inputBuffer = Vector3.zero;
	private RaycastHit grappleHit;

#endregion

#region MonoBehaviours

	private void Start ()
	{
		characterController = GetComponent<CharacterController> ();
		cameraController = GetComponent<CameraController> ();
		lineRenderer = GetComponent<LineRenderer> ();
		movementSettings = GetComponent<PlayerObject>().movementSettings;

		if (isLocalPlayer)
		{
			Subscribe ();
			CmdSetPositionAtSpawn (SpawnPointManager.Instance.SpawnPointLocation (1));
			isControllerRunning = true;
		}
	}

	private void OnDestroy ()
	{
		UnSubscribe ();
	}

	private void Update ()
	{
		GetInput ();
		if (isLocalPlayer && isControllerRunning)
		{
			if (hooked)
			{
				lineRenderer.enabled = true;
				lineRenderer.SetPosition (0, transform.position);
				lineRenderer.SetPosition (1, hookPoint);
			}
			else
			{
				lineRenderer.enabled = false;
			}
		}

		if (!jumping && characterController.isGrounded)
		{
			velocity.y = 0.0f;
		}
	}

	private void FixedUpdate ()
	{
		if (isLocalPlayer && isControllerRunning)
		{
			switch (equippedSpecial)
			{
				case SpecialMovementState.HAS_GRAPPLE_HOOK:
					{
						CmdHandleMovement_GrappleHook ();
					}
					break;
				case SpecialMovementState.HAS_JETPACK:
					{
						CmdHandleMovement_Jetpack ();
					}
					break;
			}

			inputBuffer = Vector3.zero;
		}
	}

#endregion

#region Local Functions

	private void GetInput ()
	{		
		if (isLocalPlayer && UIManager.Instance.IsPlayable())
		{
			if (Input.GetMouseButton (0))
			{
				CursorOnOff.ChangeCursorState (false);
				isControllerRunning = true;
			}

			if (Input.GetKeyDown (KeyCode.Escape))
			{
				CursorOnOff.ChangeCursorState (true);
				isControllerRunning = false;
			}
		}
	}

#endregion

#region NetCode

//	[Command]
	private void CmdHandleMovement_Jetpack ()
	{
		
		velocity.x = targetDirection.x;
		velocity.z = targetDirection.z;

		if (!jumping && !characterController.isGrounded)
		{
			velocity.y += movementSettings.gravity;
			gameObject.GetComponent<AnimatorCharacterController> ().EndSpecialMove ();
		}

		characterController.Move (velocity * Time.fixedDeltaTime);
		targetDirection = Vector3.zero;
		isMoving = false;
	}

//	[Command]
	private void CmdHandleMovement_GrappleHook ()
	{
		if (hooked)
		{
			velocity = (hookPoint - transform.position).normalized;
			velocity *= hookReelSpeed;
		}
		else
		if (locked)
		{
			velocity = Vector3.zero;
		}
		else
		{
			velocity = targetDirection;
		}

		if (!hooked && !locked && !characterController.isGrounded)
		{
			velocity.y += movementSettings.gravity;
		}

		characterController.Move (velocity * Time.fixedDeltaTime);
		targetDirection = Vector3.zero;
		isMoving = false;

		if (hooked && (Vector3.Distance (transform.position, hookPoint) <= minReelDistance))
		{
			hooked = false;
			locked = true;
		}
	}

//	[Command]
	private void CmdSetPositionAtSpawn (Vector3 position)
	{
		transform.position = position;
	}

//	[Command]
	private void CmdFindDirection (Vector3 inputVector)
	{
		isMoving = true;
		targetDirection = inputVector;
		targetDirection = transform.TransformDirection (targetDirection).normalized;
		targetDirection.x *= movementSettings.accelerationRate;
		targetDirection.z *= movementSettings.accelerationRate;
	}

//	[Command]
	private void CmdRotate (float mouseHInput)
	{
		yRotation += mouseHInput * cameraSettings.lookSensitivity * Time.deltaTime;
		transform.rotation = Quaternion.Euler (0, yRotation, 0);
	}


//	[Command]
	private void CmdJetPack ()
	{
		if (canSpecialMove)
		{
			velocity.y += jetPackHeight;
			jumping = true;
			canSpecialMove = false;
			StartCoroutine (MovementCooldown (jetPackCooldown));
			UIManager.Instance.TriggerCooldown (1);
		}
	}

//	[Command]
	private void CmdGrappleHook (Vector3 position, Vector3 forward)
	{
		if (canSpecialMove)
		{
			if (Physics.Raycast (position, forward, out grappleHit, hookDistance))
			{
				hookPoint = grappleHit.point;
				hooked = true;
				canSpecialMove = false;
				UIManager.Instance.TriggerCooldown (1);
			}
		}
	}

//	[Command]
	private void CmdReleaseHook ()
	{
		locked = false;
		StartCoroutine (MovementCooldown (hookCooldown));
	}

	private void SpecialMove ()
	{
		switch (equippedSpecial)
		{
			case SpecialMovementState.HAS_GRAPPLE_HOOK:
				{
					if (!locked)
					{
						CmdGrappleHook (cameraController.PlayerCamera.transform.position, cameraController.PlayerCamera.transform.forward);
					}
					else
					{
						CmdReleaseHook ();
					}
				}
				break;
			case SpecialMovementState.HAS_JETPACK:
				{
					CmdJetPack ();
				}
				break;
		}
	}

	private IEnumerator MovementCooldown (float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		jumping = false;
		canSpecialMove = true;
			
	}

#endregion

#region IEventListener

	public void Subscribe ()
	{
		InputManager.Instance.RegisterHandler (ReceiveBroadcast);
		PlayerObject playerObj = GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			cameraSettings = playerObj.cameraSettings;
		}
	}

	public void UnSubscribe ()
	{
		InputManager.Instance.UnRegisterHandler (ReceiveBroadcast);
		PlayerObject playerObj = GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			movementSettings = null;
			cameraSettings = null;
		}
	}

	public void ReceiveBroadcast (object sender, GameEventArgs gameEventArgs)
	{
		if (isControllerRunning)
		{
			switch (gameEventArgs.eventType)
			{
				case GameEvent.HORIZONTAL_INPUT:
					{
						inputBuffer.x = gameEventArgs.amount;

						switch (equippedSpecial)
						{
							case SpecialMovementState.HAS_GRAPPLE_HOOK:
								{
									if (!locked && !hooked)
									{
										CmdFindDirection (inputBuffer);
									}
								}
								break;
							case SpecialMovementState.HAS_JETPACK:
								{
									CmdFindDirection (inputBuffer);
								}
								break;
						}
					}
					break;
				case GameEvent.VERTICAL_INPUT:
					{
						inputBuffer.z = gameEventArgs.amount;

						switch (equippedSpecial)
						{
							case SpecialMovementState.HAS_GRAPPLE_HOOK:
								{
									if (!locked && !hooked)
									{
										CmdFindDirection (inputBuffer);
									}
								}
								break;
							case SpecialMovementState.HAS_JETPACK:
								{
									CmdFindDirection (inputBuffer);
								}
								break;
						}
					}
					break;
				case GameEvent.MOUSE_X_INPUT:
					{
						CmdRotate (gameEventArgs.amount);
					}
					break;
				case GameEvent.CHARACTER_JUMP:
					{
						SpecialMove ();
					}
					break;
			}
		}
	}

#endregion

	
}
