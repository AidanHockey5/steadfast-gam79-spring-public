﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;

[RequireComponent (typeof(NetworkIdentity))]
public class CameraController : MonoBehaviour, IEventListener
{

#region Public

	public Camera PlayerCamera
	{
		get
		{
			if (playerCamera != null)
			{
				return playerCamera;
			}
			else
			{
				return null;
			}
		}
	}

#endregion

#region Private Members

	[SerializeField]
	private Camera playerCamera = null;
	private CameraSettings cameraSettings = null;
	private float xRotation = 0;
	private float currentXRotation = 0;
	private float xRotationVelocity = 0;

#endregion

#region MonoBehaviours

	private void Start ()
	{
		if (!GetComponent<NetworkIdentity> ().isLocalPlayer)
		{
			playerCamera.enabled = false;
			playerCamera.gameObject.GetComponent<FlareLayer> ().enabled = false;
			playerCamera.gameObject.GetComponent<GUILayer> ().enabled = false;
			AudioListener listener = GetComponent<AudioListener> ();
			if(listener != null)
				listener.enabled = false;

		}
		else
		{
			Subscribe ();
		}
	}

	private void OnDestroy ()
	{
		UnSubscribe ();
	}

#endregion

#region Private Functions

	private void Look (float mouseYInput)
	{
		if (playerCamera != null)
		{
			xRotation += mouseYInput * cameraSettings.lookSensitivity * Time.deltaTime;
			xRotation = Mathf.Clamp (xRotation, cameraSettings.xLowerCameraBound, cameraSettings.xUpperCameraBound);
			playerCamera.transform.localRotation = Quaternion.Euler (-xRotation, playerCamera.transform.localRotation.y, playerCamera.transform.localRotation.z);
		}
		else
		{
			Debug.LogError ("Camera target not assigned on " + gameObject.name);
		}
	}

#endregion

#region IEventListener

	public void Subscribe ()
	{

		InputManager.Instance.RegisterHandler (ReceiveBroadcast);
		PlayerObject playerObj = GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			cameraSettings = playerObj.cameraSettings;
		}
	}

	public void UnSubscribe ()
	{

		InputManager.Instance.UnRegisterHandler (ReceiveBroadcast);
		PlayerObject playerObj = GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			cameraSettings = null;
		}
	}

	public void ReceiveBroadcast (object sender, GameEventArgs gameEventArgs)
	{
		switch (gameEventArgs.eventType)
		{
			case GameEvent.MOUSE_Y_INPUT:
				{
					Look (gameEventArgs.amount);
				}
				break;
		}
	}

#endregion

}
