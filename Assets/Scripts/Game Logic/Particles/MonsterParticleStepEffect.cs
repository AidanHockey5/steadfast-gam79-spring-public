﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ShockParticleEffect: NetworkBehaviour
{
    ParticleManager pm;
	// Use this for initialization
	void Start () 
    {
        pm = ParticleManager.Instance;
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
    public void AreaOfEffect(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);
        
        for (int i = 0; i < hitColliders.Length; i++)
        {
            pm.RpcSpawnParticle("BuildingDustOffWall", gameObject.GetComponent<BoxCollider>().transform.position, 1);
        }

    }
}
