﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Mine : NetworkBehaviour
{
    ParticleManager pm;
    public float radius = 5.0f;
    public int damage = 100;

	private void Start()
	{
        pm = ParticleManager.Instance;
		/*if(isServer)
			AudioManager.instance.RpcSound ("placeMine", gameObject);*/
	}

	[ServerCallback]
	private void OnTriggerEnter(Collider other)
    {
		if (isServer)
		{
			PlayerObject po = other.gameObject.GetComponent<PlayerObject>();

			if (po == null)
				return; 
			
			if (po.playerSettings.playerType == PlayerType.MONSTER)
			{
				po.RequestTakeDamage(GameEvent.HIT_FROM_HUMAN, damage);
				AudioManager.instance.RpcSound ("mineExplode", gameObject);
				pm.RpcSpawnParticle("Explosion_02", this.transform.position, 30);
				Destroy(gameObject);
			}
			else if (po == null)
			{
				Debug.LogError("No PlayerObject attached to other collider.");
			}
		}
    }

    [ClientRpc]
    public void RpcExplode()
    {
        /*Vector3 explosiveLocation = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosiveLocation, radius);

        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
            {
                rb.AddExplosionForce(power, explosiveLocation, radius, 3.0f);
            }
        }*/
    }
}
