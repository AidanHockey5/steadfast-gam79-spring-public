﻿public enum CharacterState
{
	NONE,
    IDLE,
    MOVING,
    ATTACKING,
	GROUNDPOUNDING,
	BREATHATTACKING,
    DEAD
}