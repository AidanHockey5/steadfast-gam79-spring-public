﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class Transition : MonoBehaviour 
{
    GameObject transition;
    GUITexture gt;
    public Color transitionColor;
    public bool fade = true;
    bool openNextScene = false;
    public float transitionSpeed = 1f;
    public string sceneToLoad = null;
    public float returnToLobbyAfterSeconds = 50f;
    float timer = 0f;
    GameObject[] audioSources;

	public AudioMixerSnapshot main;
	public AudioMixerSnapshot fadeOut;
	public float fadeTime = 3.0f;

	// Use this for initialization
	void Start () 
    {
        CreateTransitionObject();
        audioSources = GameObject.FindGameObjectsWithTag("LocalSound");
		main.TransitionTo (0.0f);
	}

    void CreateTransitionObject()
    {
        transition = new GameObject("TRANSITON");
        gt = transition.AddComponent<GUITexture>();
        transition.transform.position = new Vector3(0.5f, 0.5f, 0.5f);
        gt.color = transitionColor;
        Texture2D tx = new Texture2D(256, 256, TextureFormat.ARGB32, false);
        tx.name = "tx";
        gt.texture = tx;
    }

    void ControlFade()
    {
        if (fade)
        {
            if (gt.color.a != 1)
                gt.color = new Color(transitionColor.r, transitionColor.g, transitionColor.b, Mathf.MoveTowards(gt.color.a, 1f, Time.deltaTime * transitionSpeed));
            else
                fade = false;
            if (gt.color.a == 1f && openNextScene) //Load new scene once transition has completed
            {
                SceneManager.LoadScene(sceneToLoad);
            }
        }
        else
        {
            if (gt.color.a != 0)
                gt.color = new Color(transitionColor.r, transitionColor.g, transitionColor.b, Mathf.MoveTowards(gt.color.a, 0f, Time.deltaTime * transitionSpeed));
        }
    }

	// Update is called once per frame
	void Update () 
    {
        timer += Time.deltaTime;
		if (timer >= returnToLobbyAfterSeconds) 
		{
			fadeOut.TransitionTo (fadeTime);
			SceneManager.LoadScene (sceneToLoad);
		}
        ControlFade();
        if (Input.anyKeyDown && gt.color.a == 0f) //Allow the player to skip the opening cinematic after the fade subsides (to prevent accidental skipping)
        {
			fadeOut.TransitionTo (fadeTime);
			fade = true;
            openNextScene = true;
            transitionSpeed *= 2;
        }
        if(openNextScene)
        {
            foreach(GameObject g in audioSources)
            {
                AudioSource aud = g.GetComponent<AudioSource>();
                aud.volume = Mathf.MoveTowards(aud.volume, 0f, Time.deltaTime * 3f);
            }
        }
        if(Input.GetKeyDown(KeyCode.Home)) //Forget the transition time, just friggen load the next scene. Useful for quick debugging. Press home key.
            SceneManager.LoadScene(sceneToLoad);
	}
}
