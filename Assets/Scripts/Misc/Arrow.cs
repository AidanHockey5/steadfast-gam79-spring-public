﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour 
{
	ObjectiveManager objectiveManager = null;

	void Start () 
	{
		objectiveManager = ObjectiveManager.Instance;
	}

	void Update () 
	{
		if (objectiveManager != null) 
		{
			this.gameObject.SetActive (true);
			this.transform.LookAt (objectiveManager.CurrentObjectiveLocation ());
		} 
		else 
		{
			Debug.Log ("There is no Objective Manager in this scene!");
			this.gameObject.SetActive (false);
		}
	}
}
