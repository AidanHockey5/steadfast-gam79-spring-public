﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour 
{
	protected PlayerData _playerData;
	protected MonsterData _monsterData;

	public void UpdatePlayer(PlayerData data)
	{
		_playerData = data;
		OnDataChanged ();
	}

	public void UpdateMonster(MonsterData data)
	{
		_monsterData = data;
		OnDataChanged ();
	}

	public void TriggerCooldwon(int index)
	{
		OnTriggerCooldown (index);
	}

	public void UpdateTickets()
	{
		OnUpdateTickets ();
	}

	protected virtual void OnDataChanged() {}
	protected virtual void OnTriggerCooldown(int index) {}
	protected virtual void OnUpdateTickets() {}
}
