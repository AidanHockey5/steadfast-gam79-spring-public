﻿using UnityEngine;
using System.Collections;

public class BulletTrailController : MonoBehaviour
{

#region Public

	public WeaponSettings firingWeapon = null;

#endregion

#region Private

	private LineRenderer lineRenderer = null;
	private Vector3 positionLastFrame = Vector3.zero;

#endregion

#region MonoBehaviours

	private void Start ()
	{
		lineRenderer = GetComponent<LineRenderer> ();
	}

	private void Update ()
	{
		positionLastFrame = transform.position;
		ApplyThrust ();
		// DrawTrail ();
	}

#endregion

#region Functions

	private void ApplyThrust ()
	{
		if (firingWeapon != null)
		{
			transform.position += transform.forward * firingWeapon.bulletSettings.speed * Time.deltaTime;
		}
	}

	private void DrawTrail ()
	{
		if (lineRenderer != null)
		{
			lineRenderer.SetPosition (0, positionLastFrame);
			lineRenderer.SetPosition (1, transform.position);
		}
	}

#endregion

}
