﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using System.Collections;

namespace UnityStandardAssets.Network
{
	public class LobbyServerEntry : MonoBehaviour 
	{
		public Text serverInfoText;
		public Button joinButton;
		public Text matchName;

		private string myNetworkAddress;

		public void Populate(string networkAddress)
		{
			serverInfoText.text = networkAddress;
			myNetworkAddress = networkAddress;
		}

		public void JoinMatch()
		{
			CustomNetworkManager.Instance.JoinMatch (myNetworkAddress);
		}

		public void Update()
		{
			if (matchName.text == "")
			{
				matchName.text = "MATCH #: " + (this.transform.GetSiblingIndex() + 1).ToString("00#");
			}
		}

	}
}