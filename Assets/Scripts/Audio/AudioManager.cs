﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class AudioManager : NetworkBehaviour
{
	public static AudioManager instance = null;

	public List<AudioClip> clips;
	private Dictionary<string, AudioClip> pickedAudio;

	public List<AudioMixerGroup> mixerGroups;
	private Dictionary<string, AudioMixerGroup> pickedMixerGroup;

	public List<AudioMixerSnapshot> mixerSnapshots;
	private Dictionary<string, AudioMixerSnapshot> pickedSnapshot;

	public GameObject soundObjectPrefab = null;
	public GameObject soundObjectPrefab2D = null;
	private GameObject soundObject = null;
	private GameObject jetHover = null;
	private GameObject miniFire = null;
	private GameObject breath = null;
	private GameObject bulletSource = null;
	private GameObject pantObj = null;
	private GameObject heartObj = null;

	private AudioSource source = null;

	public bool isHit = false;
	public bool isCollapsing = false;
	public bool pepped = false;
	public bool jetIsMoving = false;
	public bool miniIsFiring = false;
	public bool isGrowling = false;
	public bool musicIsPlaying = false;
	public bool isSpeaking = false;

	public bool hospitalCanPlay = true;
	public bool nukeCanPlay = true;
	public bool baseCanPlay = true;
	public bool resCanPlay = true;
	public bool electricCanPlay = true;
	public bool pBaseCanPlay = true;

	public bool hosLowHlth = false;
	public bool nukeLowHlth = false;
	public bool baseLowHlth = false;
	public bool resLowHlth = false;
	public bool electricLowHlth = false;
	public bool pBaseLowHlth = false;

	public bool humanLowHealth = false;
	public bool humanCritHealth = false;

	private string playerType = string.Empty;

	public float humMaxDist = 3.0f;
	public float mechMaxDist = 5.0f;

	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
		DontDestroyOnLoad (gameObject);
	}

	void Start ()
	{
		GameObject[] localSounds = GameObject.FindGameObjectsWithTag ("LocalSound");

		foreach (GameObject l in localSounds)
			Destroy(l);

		pickedAudio = new Dictionary<string, AudioClip> ();
		foreach (AudioClip a in clips)
			pickedAudio.Add (a.name, a);

		pickedMixerGroup = new Dictionary<string, AudioMixerGroup> ();
		foreach (AudioMixerGroup m in mixerGroups)
			pickedMixerGroup.Add (m.name, m);

		pickedSnapshot = new Dictionary<string, AudioMixerSnapshot> ();
		foreach (AudioMixerSnapshot s in mixerSnapshots)
			pickedSnapshot.Add (s.name, s);

		pickedSnapshot ["InGameMain"].TransitionTo (0.0f);

		hospitalCanPlay = true;
		nukeCanPlay = true;
		baseCanPlay = true;
		resCanPlay = true;
		electricCanPlay = true;
		pBaseCanPlay = true;

		hosLowHlth = false;
		nukeLowHlth = false;
		baseLowHlth = false;
		resLowHlth = false;
		electricLowHlth = false;
		pBaseLowHlth = false;

		humanLowHealth = false;
		humanCritHealth = false;
	}

	public void SetPlayerType(string newPlayerType)
	{
		playerType = newPlayerType;
	}
		
	public void PepTalk ()
	{
		if (playerType == "MECH")
		{
			soundObject = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
			source = soundObject.GetComponent<AudioSource> ();
			source.spatialBlend = 0.0f;
			Play (soundObject, source, "voMechStartup", "MechComp");
		}

		if (!pepped) 
		{
			pepped = true;
			soundObject = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
			source = soundObject.GetComponent<AudioSource> ();
			source.spatialBlend = 0.0f;
			isSpeaking = true;
			StartCoroutine (PlayerFbk ("pepTalk", soundObject, source));
		}
	}

	//Configures a 3D sound
	[ClientRpc]
	public void RpcSound (string audioEvent, GameObject go)
	{
		if (soundObjectPrefab == null)
			return;
		
		soundObject = (GameObject)Instantiate (soundObjectPrefab, go.transform.position, go.transform.rotation);

		if (soundObject == null)
			return;
		
		source = soundObject.GetComponent<AudioSource> ();

		if (source == null)
			return;
		
		if (audioEvent == "damage")
		{
			if (go.tag == "Support Human") 
			{
				source = Sconfig (source, "human");
				Play (soundObject, source, "maDamage" + Random.Range (1, 6), "HumanMale");
			}

			if (go.tag == "Tech Human") 
			{
				source = Sconfig (source, "human");
				Play (soundObject, source, "fbDamage" + Random.Range (1, 7), "HumanFemale");
			}

			if (go.tag == "Light Mech" || go.tag == "Heavy Mech") 
			{
				source = Sconfig (source, "mech");
				//Play (soundObject, source, "explMedComp" + Random.Range (1, 3), "Explosions");
			}

			if (go.tag == "Monster") 
			{
				int i = Random.Range (1, 10);

				if (i <= 1)
					Play (soundObject, source, "damageMonVox" + Random.Range (1, 4), "MonVox");
				/*else
					Play (soundObject, source, "normalHit" + Random.Range (1, 4), "MonHit");*/
			}
		}

		if (audioEvent == "gunshot")
		{
			soundObject.transform.parent = go.transform;
			bulletSource = go;
			//Play (soundObject, source, "genericBullet", "Gunshots");
		}

		if (audioEvent == "casingHit")
		{
			source = Sconfig (source, "human");
			source.pitch = Random.Range (0.95f, 1.05f);
			//Play (soundObject, source, "casingHit", "Casings");
		}

		if (audioEvent == "casingBounce")
		{
			source = Sconfig (source, "human");
			source.pitch = Random.Range (0.95f, 1.05f);
			//Play (soundObject, source, "casingBounce" + Random.Range (1, 6), "Casings");
		}

		if (audioEvent == "reload")
		{
			source = Sconfig (source, "human");
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "ammoResupply1", "Reload");
		}

		if (audioEvent == "placeMine") 
		{
			source = Sconfig (source, "mech");
			//Play (soundObject, source, "mineSequence", "Mines");
		}

		if (audioEvent == "mineExplode")
		{
			//Play (soundObject, source, "explMedComp3", "Explosions");
			return;
		}

		if (audioEvent == "monsterFoot")
		{
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "newMonFoot" + Random.Range (1, 4), "MonsterFoot");
		}

		if (audioEvent == "breathCharge")
		{
			soundObject.transform.parent = go.transform;
			source.pitch = Random.Range (0.95f, 1.05f);
			//Play (soundObject, source, "brthChrg", "ChrgUp");
		}

		if (audioEvent == "breathAttack")
		{
			pickedSnapshot ["MonsterDefault"].TransitionTo (0.0f);
			soundObject.transform.parent = go.transform;
			breath = soundObject;
			//Play (soundObject, source, "brthLavaLoop", "SpecialAtk");
			if (isServer)
				RpcSound ("roar", go);
		}

		if (audioEvent == "endBreath") 
		{
			pickedSnapshot ["BreathFade"].TransitionTo (2.0f);
			Destroy (breath, 2.0f);
		}

		if (audioEvent == "monsterExertL")
		{
			if (!isGrowling)
			{
				soundObject.transform.parent = go.transform;
				//Play (soundObject, source, "exertMonVox" + Random.Range (1, 3), "MonVox");
			}
			else
				Destroy (soundObject);
		}

		if (audioEvent == "monsterExertS")
		{
			if (!isGrowling)
			{
				soundObject.transform.parent = go.transform;
				//Play (soundObject, source, "shExertMonVox" + Random.Range (1, 3), "MonVox");
			}
			else
				Destroy (soundObject);
		}

		if (audioEvent == "monsterAtkCharge")
		{
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "matkChrg", "ChrgUp");
		}

		if (audioEvent == "monsterSwipe")
		{
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "matkSwp" + Random.Range (1, 3), "BasicAtk");
		}

		if (audioEvent == "monsterPound")
		{
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "matkPnd" + Random.Range (1, 4), "BasicAtk");
		}

		if (audioEvent == "monsterStmp")
		{
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "matkStmp", "BasicAtk");
		}

		if (audioEvent == "roar")
		{
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "roarMonVox", "MonVox");
		}

		if (audioEvent == "monDamage")
		{
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "damageMonVox" + Random.Range (1, 4), "MonVox");
		}

		if (audioEvent == "mechFoot")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "mech");
			source.pitch = Random.Range (0.95f, 1.05f);

			/*if (go.tag == "Light Mech")
				Play (soundObject, source, "mechFootMet" + Random.Range (1, 3), "LightMechFoot");
			if (go.tag == "Heavy Mech")
				Play (soundObject, source, "mechFootHvy" + Random.Range (1, 3), "MechFoot");*/
		}

		if (audioEvent == "servoHiss")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "mech");
			//Play (soundObject, source, "servoStop", "MechMove");
		}

		if (audioEvent == "death")
		{
			soundObject.transform.parent = go.transform;

			if (go.tag == "Support Human") 
			{
				source = Sconfig (source, "human");
				Play (soundObject, source, "maExp" + Random.Range (1, 6), "HumanMale");
			} 
			else if (go.tag == "Tech Human") 
			{
				source = Sconfig (source, "human");
				Play (soundObject, source, "fbExp" + Random.Range (1, 7), "HumanFemale");
			} 
			else if (go.tag == "Monster") 
			{
				Play (soundObject, source, "howlMonVox", "MonVox");
			}
			else if (go.tag == "Light Mech" || go.tag == "Heavy Mech") 
			{
				source = Sconfig (source, "mech");
				Play (soundObject, source, "voMechShutdown", "MechComp");
			}
			else
				Destroy (soundObject);
		}

		if (audioEvent == "mechDeath")
		{
			/*soundObject.transform.parent = go.transform;

			if (go.tag == "Light Mech")
			{
				source.spatialBlend = 0.0f;
				Play (soundObject, source, "voMechShutdown", "MechComp");
			}

			if (go.tag == "Heavy Mech")
			{
				source.spatialBlend = 0.0f;
				Play (soundObject, source, "voMechShutdown", "MechComp");
			}*/
			Destroy (soundObject);
		}

		if (audioEvent == "mechImpact")
		{
			source = Sconfig (source, "mech");
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "mechFootLt" + Random.Range (1, 3), "MechImpact");
		}

		if (audioEvent == "jetpackStart")
		{
			source = Sconfig (source, "mech");
			if (pickedSnapshot.ContainsKey ("MechDefault"))
			{				
				pickedSnapshot ["MechDefault"].TransitionTo (0.0f);
				soundObject.transform.parent = go.transform;
				//Play (soundObject, source, "jetStart", "MechJetpack");
				if (isServer)
					RpcSound ("jetpackHover", go);
				jetIsMoving = true;
			}
		}

		if (audioEvent == "jetpackHover")
		{
			source = Sconfig (source, "mech");
			jetHover = soundObject;
			jetHover.transform.parent = go.transform;
			jetHover.GetComponent<AudioSource> ().loop = true;
			//Play (jetHover, source, "jetLoop", "MechJetHover");
		}

		if (audioEvent == "jetpackStop")
		{
			if (pickedSnapshot.ContainsKey ("HoverFade"))
			{
				soundObject.transform.parent = go.transform;
				source = Sconfig (source, "mech");
				//Play (soundObject, source, "jetEnd", "MechJetpack");
				pickedSnapshot ["HoverFade"].TransitionTo (1.0f);
				Destroy (jetHover, 1.5f);
			}
		}

		if (audioEvent == "jetpackMove")
			jetHover.GetComponent<AudioSource> ().pitch = 1.05f;

		if (audioEvent == "jetpackIdle")
			jetHover.GetComponent<AudioSource> ().pitch = 1.00f;

		if (audioEvent == "servoMove")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "mech");
			source.pitch = Random.Range (0.95f, 1.05f);
			//Play (soundObject, source, "servoLoop", "MechMove");
		}

		if (audioEvent == "specialGunShoot")
		{

			if (go.tag == "Light Mech")
			{
				source.volume = 1.25f;
				soundObject.transform.parent = go.transform;
				//Play (soundObject, source, "gaussFire", "MechWeapons");
			}

			if (go.tag == "Heavy Mech")
			{
				soundObject.transform.parent = go.transform;
				//Play (soundObject, source, "cannonRifle", "MechWeapons");
			}

			if (go.tag == "Support Human")
			{
				soundObject.transform.parent = go.transform;
				//Play (soundObject, source, "hypoGunshot", "HumanWeapons");
			}

			if (go.tag == "Tech Human")
			{
				soundObject.transform.parent = go.transform;
				//Play (soundObject, source, "railGunshot", "HumanWeapons");
			}
		}

		if (audioEvent == "minigun")
		{
			soundObject.transform.parent = go.transform;

			StartCoroutine (SoundSeq (audioEvent, go));
			/*if (!miniIsFiring)
			{
				if (pickedSnapshot.ContainsKey ("MechDefault"))
				{
					pickedSnapshot ["MechDefault"].TransitionTo (0.0f);
					miniFire = soundObject;
					miniFire.GetComponent<AudioSource> ().loop = true;
					Play (soundObject, source, "minigunLoop", "MiniFire");
					miniIsFiring = true;
				}
			}

			if (miniIsFiring)
			{
				Play (soundObject, source, "minigunEnd", "MinigunSpin");
				if (pickedSnapshot.ContainsKey ("MiniFade"))
				{
					pickedSnapshot ["MiniFade"].TransitionTo (0.5f);
					Destroy (miniFire, 0.5f);
					miniIsFiring = false;
				}
			}*/
		}

		if (audioEvent == "rocketLaunch")
		{
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "missileLaunch", "MechWeapons");
		}

		if (audioEvent == "mechDamage")
		{
			source = Sconfig (source, "mech");
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "explLtComp" + Random.Range (1, 3), "Explosions");
		}

		if (audioEvent == "explosionLarge")
		{
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "explHvyComp" + Random.Range (1, 3), "Explosions");
		}

		if (audioEvent == "servoShort")
		{
			source = Sconfig (source, "mech");
			soundObject.transform.parent = go.transform;
			//Play (soundObject, source, "servoLoop", "MechMove");
		}

		if (audioEvent == "railgunCharge")
		{
			StartCoroutine (SoundSeq ("railgunCharge", go));
			/*RpcSound ("specialGunShoot", go);
			source = Sconfig (source, "mech");
			soundObject.transform.parent = go.transform;
			Play (soundObject, source, "gaussCharge", "Railgun");*/
		}

		if (audioEvent == "humanFoot")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "human");
			source.pitch = Random.Range (0.95f, 1.05f);
			//Play (soundObject, source, "footstepConcrete" + Random.Range (1, 6), "HumanFoot");
		}

		if (audioEvent == "damageFallHit")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "human");

			if (go.tag == "Support Human")
				Play (soundObject, source, "maDamage" + Random.Range (1, 3), "HumanMale");
			
			if (go.tag == "Tech Human")
				Play (soundObject, source, "fbDamage" + Random.Range (1, 6), "HumanFemale");
		}

		if (audioEvent == "damageFallLand")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "human");

			if (go.tag == "Support Human")
				Play (soundObject, source, "maDamage" + Random.Range (4, 6), "HumanMale");

			if (go.tag == "Tech Human")
				Play (soundObject, source, "fbDamage7", "HumanFemale");
		}

		if (audioEvent == "bodyImpact")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "human");
			//Play (soundObject, source, "bdyImp" + Random.Range (1, 6), "BodyImpact");
		}

		if (audioEvent == "getUp")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "human");

			if (go.tag == "SupportHuman")
				Play (soundObject, source, "maExert9", "HumanMale");

			if (go.tag == "Tech Human")
				Play (soundObject, source, "fbExert2", "HumanFemale");
		}

		if (audioEvent == "pickUp")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "human");
			//Play (soundObject, source, "ammoResupply3", "Reload");
		}

		if (audioEvent == "grappleFire")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "human");
			//Play (soundObject, source, "grapFire", "Grappler");
			soundObject = (GameObject)Instantiate (soundObjectPrefab, go.transform.position, go.transform.rotation);
			soundObject.transform.parent = go.transform;
			source = soundObject.GetComponent<AudioSource> ();
			source = Sconfig (source, "human");
			//Play (soundObject, source, "grapExt", "Grappler");
		}

		if (audioEvent == "growl")
		{
			int i = Random.Range (1, 10);
			if (i == 1)
				StartCoroutine (SoundSeq (audioEvent, go));
			else
				Destroy (soundObject);
		}

		if (audioEvent == "humanLowHealth") 
		{
			source = Sconfig (source, "human");
			source.loop = true;
			pantObj = soundObject;

			if (go.tag == "Support Human") 
			{
				Play (pantObj, source, "maPant", "Panting");
				pickedSnapshot ["HumLowHlth"].TransitionTo (1.0f);
			}
			if (go.tag == "Tech Human") 
			{
				Play (pantObj, source, "fbPant", "Panting");
				pickedSnapshot ["HumLowHlth"].TransitionTo (1.0f);
			}
		}

		if (audioEvent == "humanCritHealth") 
		{
			source = Sconfig (source, "human");
			source.loop = true;
			heartObj = soundObject;
			//Play (heartObj, source, "heartbeat", "Heartbeat");
			pickedSnapshot ["HumCritHlth"].TransitionTo (1.0f);
		}

		if (audioEvent == "humanGoodHealth") 
		{
			pickedSnapshot ["HumanDefault"].TransitionTo (1.0f);
			Destroy (pantObj, 1.5f);
			Destroy (heartObj, 1.5f);
		}

		if (audioEvent == "monLowHealth") 
			StartCoroutine(SoundSeq(audioEvent, go));

		if (audioEvent == "monCritHealth") 
		{
			soundObject.transform.parent = go.transform;
			source.loop = true;
			//Play (soundObject, source, "monPant", "MonCrit");
			pantObj = soundObject;
			if (isServer)
				RpcSound ("monHeart", go);
		}

		if (audioEvent == "monHeart")
		{
			soundObject.transform.parent = go.transform;
			source = Sconfig (source, "mech");
			source.loop = true;
			source.pitch = 0.8f;
			heartObj = soundObject;
			//Play (heartObj, source, "heartbeat", "MonCrit");
			pickedSnapshot ["MonCritHlth"].TransitionTo (1.0f);
		}

		if (audioEvent == "monGoodHealth") 
		{
			pickedSnapshot ["MonsterDefault"].TransitionTo (1.0f);
			Destroy (pantObj, 1.5f);
			Destroy (heartObj, 1.5f);
		}
	}

	public AudioSource Sconfig (AudioSource source, string sourceType)
	{
		if (sourceType == "human") 
		{
			source.rolloffMode = AudioRolloffMode.Linear;
			source.minDistance = 1.0f;
			source.maxDistance = humMaxDist;
		}

		if (sourceType == "mech") 
		{
			source.rolloffMode = AudioRolloffMode.Linear;
			source.minDistance = 1.0f;
			source.maxDistance = mechMaxDist;
		}

		return (source);
	}

	public void PlayerFbk (string audioEvent)
	{
		if (isServer)
			RpcSound2D (audioEvent);
	}

	//Configures a 2D sound
	[ClientRpc]
	public void RpcSound2D (string audioEvent)
	{
		soundObject = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
		source = soundObject.GetComponent<AudioSource> ();
		source.spatialize = false;
		source.spatialBlend = 0.0f;

		if (audioEvent == "battleMusic1" && !musicIsPlaying) 
		{
			source.loop = true;
			Play (soundObject, source, "musBattle1", "MX");
			musicIsPlaying = true;
		}

		if (audioEvent == "battleMusic2" && !musicIsPlaying)
		{
			source.loop = true;
			Play (soundObject, source, "musBattle2", "MX");
			musicIsPlaying = true;
		}

		if (!isSpeaking) 
		{
			if (audioEvent == "pepTalk" && !pepped) {
				isSpeaking = true;
				StartCoroutine (PlayerFbk ("pepTalk", soundObject, source));
			}

			if (audioEvent == "HOSPITAL") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "MILITARYBASE") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "POWERPLANT") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "NUCLEARPLANT") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "RESEARCHLAB") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "PLAYERBASE") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}


			if (audioEvent == "HOSPITALlow") 
			{
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "MILITARYBASElow") 
			{
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "POWERPLANTlow") 
			{
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "NUCLEARPLANTlow") 
			{
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "RESEARCHLABlow") 
			{
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "PLAYERBASElow") 
			{
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "objDest") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "mechStart") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "mechLowHealth") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "mechCritHealth") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk (audioEvent, soundObject, source));
			}

			if (audioEvent == "gameOver") {
				isSpeaking = true;
				StartCoroutine (PlayerFbk ("gameOver", soundObject, source));
			}
		}

		if (audioEvent == "mechStart")
			Play (soundObject, source, "voMechStartup", "MechComp");
	}

	//Configures building destruction sounds
	public void BldgSoundConfig (string audioEvent, string buildingType, Vector3 pos, float fadeTime)
	{
		if (audioEvent == "hit")
		{
			if (!isHit)
			{
				StartCoroutine (Regulate ("hit", 1.0f, buildingType, pos));

				if (buildingType == "HIGHRISE")
				{
					if (pickedSnapshot.ContainsKey ("HitHighrise"))
						pickedSnapshot ["HitHighrise"].TransitionTo (0.0f);
				}
				else if (buildingType == "LARGE_CITY")
				{
					if (pickedSnapshot.ContainsKey ("HitLargeCity"))
						pickedSnapshot ["HitLargeCity"].TransitionTo (0.0f);
				}
				else if (buildingType == "APARTMENT")
				{
					if (pickedSnapshot.ContainsKey ("HitApartment"))
						pickedSnapshot ["HitApartment"].TransitionTo (0.0f);
				}
				else if (buildingType == "OFFICE")
				{
					if (pickedSnapshot.ContainsKey ("HitOffice"))
						pickedSnapshot ["HitOffice"].TransitionTo (0.0f);
				}
				else if (buildingType == "HOUSE")
				{
					if (pickedSnapshot.ContainsKey ("HitHouse"))
						pickedSnapshot ["HitHouse"].TransitionTo (0.0f);
				}
				else if (buildingType == "WAREHOUSE")
				{
					if (pickedSnapshot.ContainsKey ("HitWarehouse"))
						pickedSnapshot ["HitWarehouse"].TransitionTo (0.0f);
				}
				else
				{
					if (pickedSnapshot.ContainsKey ("HitHighrise"))
						pickedSnapshot ["HitHighrise"].TransitionTo (0.0f);
				}

				if (isServer)
				{
					pos = GameObject.FindGameObjectWithTag ("Monster").transform.position;
					RpcSoundBldg ("bldgHitGlass" + Random.Range (1, 3), "HitGlassLyr", pos);
					RpcSoundBldg ("bldgHitDebris" + Random.Range (1, 3), "HitDebrisLyr", pos);
					RpcSoundBldg ("bldgHitMetal" + Random.Range (1, 3), "HitMetalLyr", pos);
					RpcSoundBldg ("bldgHitImpact" + Random.Range (1, 3), "HitImpactLyr", pos);
					RpcSoundBldg ("bldgHitLFE", "HitLFE", pos);
				}
			}
		}

		if (audioEvent == "collapse")
		{
			if (!isCollapsing)
			{
				StartCoroutine (Regulate ("collapse", fadeTime, buildingType, pos));

				if (buildingType == "HIGHRISE")
				{
					if (pickedSnapshot.ContainsKey ("ColHighrise"))
						pickedSnapshot ["ColHighrise"].TransitionTo (0.0f);
				}
				else if (buildingType == "LARGE_CITY")
				{
					if (pickedSnapshot.ContainsKey ("ColLargeCity"))
						pickedSnapshot ["ColLargeCity"].TransitionTo (0.0f);
				}
				else if (buildingType == "APARTMENT")
				{
					if (pickedSnapshot.ContainsKey ("ColApartment"))
						pickedSnapshot ["ColApartment"].TransitionTo (0.0f);
				}
				else if (buildingType == "OFFICE")
				{
					if (pickedSnapshot.ContainsKey ("ColOffice"))
						pickedSnapshot ["ColOffice"].TransitionTo (0.0f);
				}
				else if (buildingType == "HOUSE")
				{
					if (pickedSnapshot.ContainsKey ("ColHouse"))
						pickedSnapshot ["ColHouse"].TransitionTo (0.0f);
				}
				else if (buildingType == "WAREHOUSE")
				{
					if (pickedSnapshot.ContainsKey ("ColWarehouse"))
						pickedSnapshot ["ColWarehouse"].TransitionTo (0.0f);
				}
				else
				{
					if (pickedSnapshot.ContainsKey ("ColHighrise"))
						pickedSnapshot ["ColHighrise"].TransitionTo (0.0f);
				}

				if (isServer)
				{
					pos = GameObject.FindGameObjectWithTag ("Monster").transform.position;
					RpcSoundBldg ("bldgColGlass" + Random.Range (1, 3), "ColGlassLyr", pos);
					RpcSoundBldg ("bldgColHiCrack" + Random.Range (1, 3), "ColHiCrackLyr", pos);
					RpcSoundBldg ("bldgColLoCrack" + Random.Range (1, 3), "ColLoCrackLyr", pos);
					RpcSoundBldg ("bldgColMetal" + Random.Range (1, 3), "ColMetalLyr", pos);
					RpcSoundBldg ("bldgColRock" + Random.Range (1, 3), "ColRockLyr", pos);
					RpcSoundBldg ("bldgColLFE", "ColLFE", pos);
				}
			}
		}
	}

	//Plays the sound
	public void Play (GameObject soundObject, AudioSource source, string sound, string mixer)
	{
		if (source == null)
		{
			print (sound + " Audio Source is null.");
			return;
		}

		if (pickedAudio == null)
		{
			print (sound + " clip is null.");
			return;
		}

		AudioClip clip;

		if (!pickedAudio.TryGetValue (sound, out clip)) 
		{
			print (sound + " clip not in list.");
			return;
		}
					
		source.clip = clip;

		AudioMixerGroup aGroup;

		if (!pickedMixerGroup.TryGetValue (mixer, out aGroup)) 
		{
			print (mixer + " mixer not in list.");
			return;
		}
		
		source.outputAudioMixerGroup = aGroup;
		source.Play ();

		if (!source.loop) 
			Destroy (soundObject, source.clip.length);
	}

	//Plays building destruction sounds
	[ClientRpc]
	public void RpcSoundBldg (string sound, string mixer, Vector3 pos)
	{
		soundObject = (GameObject)Instantiate (soundObjectPrefab, pos, Quaternion.identity);
		source = soundObject.GetComponent<AudioSource> ();

		//Play (soundObject, source, sound, mixer);
	}

	//Regulates timing of building collapse
	IEnumerator Regulate (string audioEvent, float time, string type, Vector3 pos)
	{
		if (audioEvent == "hit")
		{
			isHit = true;
			yield return new WaitForSeconds (time);
			isHit = false;
		}
		if (audioEvent == "collapse")
		{
			isCollapsing = true;
			yield return new WaitForSeconds (time);
			if (isServer)
				RpcSoundBldg ("bldgSettle" + Random.Range (1, 4), "Settle", pos);
			if (pickedSnapshot.ContainsKey ("ColFadeOut")) 
			{
				if (type == "HIGHRISE" || type == "LARGE_CITY" || type == "APARTMENT")
					pickedSnapshot ["ColFadeOut"].TransitionTo (8.0f);
				else if (type == "OFFICE" || type == "WAREHOUSE" || type == "HOUSE")
					pickedSnapshot ["ColFadeOut"].TransitionTo (3.0f);
				else
					pickedSnapshot ["ColFadeOut"].TransitionTo (5.0f);
			}
			isCollapsing = false;
		}
	}

	//Regulates timeing and playback of various sound sequences
	IEnumerator SoundSeq (string audioEvent, GameObject go)
	{
		if (!isGrowling) 
		{
			if (audioEvent == "growl") 
			{
				isGrowling = true;
				soundObject.transform.parent = go.transform;
				Play (soundObject, source, "growlMonVox", "MonVox");
				yield return new WaitForSeconds (pickedAudio ["growlMonVox"].length);
				isGrowling = false;
			}

			if (audioEvent == "monLowHealth") 
			{
				isGrowling = true;
				soundObject.transform.parent = go.transform;
				Play (soundObject, source, "enragedMonVox" + Random.Range(1,3), "MonVox");
				yield return new WaitForSeconds (pickedAudio ["enragedMonVox1"].length + 1.0f);
				isGrowling = false;
			}

			if (audioEvent == "monCritHealth") 
			{
				isGrowling = true;
				soundObject.transform.parent = go.transform;
				Play (soundObject, source, "injuredMonVox" + Random.Range(1,3), "MonVox");
				yield return new WaitForSeconds (pickedAudio ["injuredMonVox1"].length + 1.0f);
				isGrowling = false;
				yield return new WaitForSeconds (10.0f);
				RpcSound ("monCritHealth", go);
			}

			if (audioEvent == "minigun")
			{
				soundObject.transform.parent = go.transform;
				pickedSnapshot ["MechDefault"].TransitionTo (0.0f);
				//soundObject.GetComponent<AudioSource> ().loop = true;
				//Play (soundObject, source, "minigunLoop", "MiniFire");
				//yield return new WaitForSeconds (pickedAudio ["minigunLoop"].length / 2);
				//pickedSnapshot ["MiniFade"].TransitionTo (pickedAudio ["minigunLoop"].length / 2);
			}

			if (audioEvent == "railgunCharge") 
			{
				source = Sconfig (source, "mech");
				soundObject.transform.parent = go.transform;
				//Play (soundObject, source, "gaussCharge", "Railgun");
				//yield return new WaitForSeconds (pickedAudio ["gaussCharge"].length);
				RpcSound ("specialGunShoot", go);
			}
		}
	}

	//Regulates timeing and playback of player feedback
	IEnumerator PlayerFbk (string audioEvent, GameObject soundObject, AudioSource source)
	{
		if (audioEvent == "pepTalk") 
		{
			//Play (soundObject, source, "channelOpen", "Radio");
			//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
			GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
			source = soundObjectTwo.GetComponent<AudioSource> ();
			Play (soundObjectTwo, source, "voPepTalk", "PlayerFeedback");
			yield return new WaitForSeconds (pickedAudio ["voPepTalk"].length);
			isSpeaking = false;
			pepped = true;
		}

		else if (audioEvent == "HOSPITAL")
		{
			if (hospitalCanPlay == true) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voHospital", "PlayerFeedback");
				hospitalCanPlay = false;
				yield return new WaitForSeconds (pickedAudio ["voHospital"].length + 0.5f);
				isSpeaking = false;
				yield return new WaitForSeconds (120.0f);
				hospitalCanPlay = true;
			}
		}
			
		else if (audioEvent == "MILITARYBASE")
		{
			if (baseCanPlay == true) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voMilitaryBase", "PlayerFeedback");
				baseCanPlay = false;
				yield return new WaitForSeconds (pickedAudio ["voMilitaryBase"].length + 0.5f);
				isSpeaking = false;
				yield return new WaitForSeconds (120.0f);
				baseCanPlay = true;
			}
		}

		else if (audioEvent == "NUCLEARPLANT")
		{
			if (nukeCanPlay == true) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voNuke", "PlayerFeedback");
				nukeCanPlay = false;
				yield return new WaitForSeconds (pickedAudio ["voNuke"].length + 0.5f);
				isSpeaking = false;
				yield return new WaitForSeconds (120.0f);
				nukeCanPlay = true;
			}
		}

		else if (audioEvent == "POWERPLANT")
		{
			if (electricCanPlay == true) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voElectric", "PlayerFeedback");
				electricCanPlay = false;
				yield return new WaitForSeconds (pickedAudio ["voElectric"].length + 0.5f);
				isSpeaking = false;
				yield return new WaitForSeconds (120.0f);
				electricCanPlay = true;
			}
		}

		else if (audioEvent == "RESEARCHLAB")
		{
			if (resCanPlay == true) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voResearch", "PlayerFeedback");
				resCanPlay = false;
				yield return new WaitForSeconds (pickedAudio ["voResearch"].length + 0.5f);
				isSpeaking = false;
				yield return new WaitForSeconds (120.0f);
				resCanPlay = true;
			}
		}

		else if (audioEvent == "PLAYERBASE")
		{
			if (pBaseCanPlay == true) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voPlayerBase", "PlayerFeedback");
				pBaseCanPlay = false;
				yield return new WaitForSeconds (pickedAudio ["voPlayerBase"].length + 0.5f);
				isSpeaking = false;
				yield return new WaitForSeconds (120.0f);
				pBaseCanPlay = true;
			}
		}

		else if (audioEvent == "gameOver") 
		{
			pickedSnapshot ["GameOver1"].TransitionTo (5.0f);
			yield return new WaitForSeconds (8.0f);
			pickedSnapshot ["GameOver2"].TransitionTo (3.0f);
			isSpeaking = false;
		}

		else if (audioEvent == "HOSPITALlow") 
		{
			if (!hosLowHlth) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voShowUp", "PlayerFeedback");
				hosLowHlth = true;
				yield return new WaitForSeconds (pickedAudio ["voShowUp"].length + 0.5f);
				isSpeaking = false;
			} 
			else 
			{
				Destroy (soundObject);
				isSpeaking = false;
			}
		}

		else if (audioEvent == "MILITARYBASElow") 
		{
			if (!baseLowHlth) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voShowUp", "PlayerFeedback");
				baseLowHlth = true;
				yield return new WaitForSeconds (pickedAudio ["voShowUp"].length + 0.5f);
				isSpeaking = false;
			}
			else 
			{
				Destroy (soundObject);
				isSpeaking = false;
			}
		}

		else if (audioEvent == "POWERPLANTlow") 
		{
			if (!electricLowHlth) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voShowUp", "PlayerFeedback");
				electricLowHlth = true;
				yield return new WaitForSeconds (pickedAudio ["voShowUp"].length + 0.5f);
				isSpeaking = false;
			}
			else 
			{
				Destroy (soundObject);
				isSpeaking = false;
			}
		}

		else if (audioEvent == "NUCLEARPLANTlow") 
		{
			if (!nukeLowHlth) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voShowUp", "PlayerFeedback");
				nukeLowHlth = true;
				yield return new WaitForSeconds (pickedAudio ["voShowUp"].length + 0.5f);
				isSpeaking = false;
			}
			else 
			{
				Destroy (soundObject);
				isSpeaking = false;
			}
		}

		else if (audioEvent == "RESEARCHLABlow") 
		{
			if (!resLowHlth) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voShowUp", "PlayerFeedback");
				resLowHlth = true;
				yield return new WaitForSeconds (pickedAudio ["voShowUp"].length + 0.5f);
				isSpeaking = false;
			}
			else 
			{
				Destroy (soundObject);
				isSpeaking = false;
			}
		}

		else if (audioEvent == "PLAYERBASElow") 
		{
			if (!pBaseLowHlth) 
			{
				//Play (soundObject, source, "channelOpen", "Radio");
				//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
				GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
				source = soundObjectTwo.GetComponent<AudioSource> ();
				Play (soundObjectTwo, source, "voShowUp", "PlayerFeedback");
				pBaseLowHlth = true;
				yield return new WaitForSeconds (pickedAudio ["voShowUp"].length + 0.5f);
				isSpeaking = false;
			}
			else 
			{
				Destroy (soundObject);
				isSpeaking = false;
			}
		}

		else if (audioEvent == "objDest") 
		{
			//Play (soundObject, source, "channelOpen", "Radio");
			//yield return new WaitForSeconds (pickedAudio ["channelOpen"].length - 0.5f);
			GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
			source = soundObjectTwo.GetComponent<AudioSource> ();
			Play (soundObject, source, "voObjDestroy", "PlayerFeedback");
			yield return new WaitForSeconds (pickedAudio ["voObjDestroy"].length + 0.5f);
			isSpeaking = false;
		}

		else if (audioEvent == "mechLowHealth")
		{

			//Play (soundObject, source, "mechAlarm", "Alarm");
			GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
			source = soundObjectTwo.GetComponent<AudioSource> ();
			Play (soundObjectTwo, source, "voMechShieldsGone", "MechComp");
			yield return new WaitForSeconds (pickedAudio ["voMechShieldsGone"].length + 0.5f);
			isSpeaking = false;
		}

		else if (audioEvent == "mechCritHealth") 
		{
			//Play (soundObject, source, "mechAlarm", "Alarm");
			GameObject soundObjectTwo = (GameObject)Instantiate (soundObjectPrefab2D, transform.position, transform.rotation);
			source = soundObjectTwo.GetComponent<AudioSource> ();
			Play (soundObjectTwo, source, "voMechCritical", "MechComp");
			yield return new WaitForSeconds (pickedAudio ["voMechCritical"].length + 0.5f);
			isSpeaking = false;
		}
		else 
		{
			Destroy (soundObject);
			isSpeaking = false;
		}

		isSpeaking = false;
	}
}