﻿//A fancy camera designed to fly through the main level as a background effect for the title screen
//Programmed by Aidan Lawrence
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class FlyThroughTargets //Data payload for camera paths
{
    public Transform[] pathPoints;
    public Transform focusPoint;
    public float speed = 1f;
    public bool doTransition = true;
}

public class FlyThroughCamera : MonoBehaviour 
{
	Camera cam;
	int index = 0;
	//public float movementSpeed = 10f;
	public List<FlyThroughTargets> paths = new List<FlyThroughTargets>();
	GameObject transition;
	GUITexture gt;
	bool fade = false;
    float maxDistance = 0; //Distance between the camera and the last possible path point.
    public Color transitionColor = Color.black;
    bool validPaths = false;
    void Start () 
	{
        validPaths = CheckForValidPaths();
        if (GetComponent<Camera>() != null) //Check if on camera object. If not, set one up.
            cam = GetComponent<Camera>();
        else
        {
            cam = gameObject.AddComponent<Camera>();
            gameObject.AddComponent<GUILayer>();
            gameObject.AddComponent<AudioListener>();
        }
        if(validPaths)
        {
            transform.position = paths[0].pathPoints[0].position;
            CreateTransitionObject();
            maxDistance = GetMaximumDistance();
        }
    }


	void CreateTransitionObject()
	{
		transition = new GameObject ("TRANSITON");
		gt = transition.AddComponent<GUITexture> ();
		transition.transform.position = new Vector3 (0.5f, 0.5f, 0.5f);
        gt.color = transitionColor;
        Texture2D tx = new Texture2D (256, 256, TextureFormat.ARGB32, false);
        tx.name = "tx";
        gt.texture = tx;
    }
	
	// Update is called once per frame
	void Update () 
	{
        if(validPaths)
        {
            MoveCamera();
            Fade();
        }
	}

	void Fade()
	{
		if (fade) 
		{
			if (gt.color.a != 1) 
				gt.color = new Color(transitionColor.r, transitionColor.g, transitionColor.b, Mathf.MoveTowards(gt.color.a, 1f, Time.deltaTime*2f));
			else
				fade = false;
		}
		else
		{
			if (gt.color.a != 0) 
				gt.color = new Color(transitionColor.r, transitionColor.g, transitionColor.b, Mathf.MoveTowards(gt.color.a, 0f, Time.deltaTime*2f));
		}
	}


	int targetIndex = 0; //Index of each target 
	void MoveCamera()
	{
		cam.transform.LookAt (paths[index].focusPoint);
		gameObject.transform.position = Vector3.MoveTowards (gameObject.transform.position, paths[index].pathPoints[targetIndex].position, Time.deltaTime * paths[index].speed);

		float distance = Vector3.Distance (gameObject.transform.position, paths[index].pathPoints[targetIndex].position);

		if (distance <= 3f) 
		{
			targetIndex++;
			if (targetIndex >= paths[index].pathPoints.Length) //Move on to the next set of paths
			{
				targetIndex = 0;
				index++;
                if (index >= paths.Count) //Reset the camera to the original path
				{
					index = 0;
					transform.position = paths[0].pathPoints[0].position;
				} 
				else 
				{
					transform.position = paths[index].pathPoints[targetIndex].position;
				}
                maxDistance = GetMaximumDistance();
            }
		}
        if (GetMaximumDistance() <= maxDistance * 0.1f && paths[index].doTransition) //Fade the camera out if there is less than 10% distance left to the furthest transform
            fade = true;
    }

    float GetMaximumDistance()
    {
        int maxTIndex = paths[index].pathPoints.Length - 1;
        return Vector3.Distance(gameObject.transform.position, paths[index].pathPoints[maxTIndex].position);
    }

    bool CheckForValidPaths() //Failsafe that validates all fly-through paths.
    {
        if (paths.Count == 0)
        {
            Debug.LogError("There are no paths set up on the fly-through camera. Stopping fly-through");
            return false;
        }
        foreach (FlyThroughTargets ftt in paths)
        {
            if (ftt.pathPoints.Length == 0)
            {
                Debug.LogError("There is a path that does not contain any transforms! Stopping fly-through");
                return false;
            }
            if (ftt.focusPoint == null)
            {
                Debug.LogError("There is a path that is missing it's focus point! Stopping fly-through");
            }
            foreach (Transform t in ftt.pathPoints)
            {
                if (t == null)
                {
                    Debug.LogError("There is a path that contains a null transform! Stopping fly-through");
                    return false;
                }
            }
        }
        return true;
    }
}
