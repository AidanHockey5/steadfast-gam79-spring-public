﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class MonsterAbilityController : NetworkBehaviour, IEventListener
{

#region Private

	[Header ("Attack Area Settings")]
	[SerializeField]
	[Tooltip ("Index Order Matters!")]
	private GameObject[] attackAreas;
	private MonsterAbilitySettings[] monsterAbility;
	private float timeCounter;

#endregion

#region MonoBehaviours

	private void Start ()
	{
		Subscribe ();

		if (monsterAbility != null)
		{
			for (int i = 0; i < attackAreas.Length; i++)
			{
				attackAreas [i].GetComponent<AttackAreaController> ().monsterAbilitySettings = monsterAbility [i];
			}
		}
	}

#endregion

#region IEventListener

	public void Subscribe ()
	{
		InputManager.Instance.RegisterHandler (ReceiveBroadcast);
		PlayerObject playerObj = GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			monsterAbility = playerObj.monsterAbilitySettings;
		}
	}

	public void UnSubscribe ()
	{
		InputManager.Instance.UnRegisterHandler (ReceiveBroadcast);
		PlayerObject playerObj = GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			monsterAbility = null;
		}
	}

	public void ReceiveBroadcast (object sender, GameEventArgs gameEventArgs)
	{

	}

#endregion

#region Animation Event Functions

	public void AttackAreaOn (int attackAreaIndex)
	{
		attackAreas [attackAreaIndex].SetActive (true);
	}

	public void AttackAreaOff (int attackAreaIndex)
	{
		attackAreas [attackAreaIndex].SetActive (false);
	}

	public void TimeOfAttack (int attackAreaIndex)
	{
		AttackAreaController attackArea = attackAreas [attackAreaIndex].GetComponent<AttackAreaController> ();

		if (attackArea != null)
		{
			if (attackArea.monsterAbilitySettings.isDamageOverTime)
			{
				attackArea.isDamaging = true;
			}
			else
			{
				attackArea.InstantDamage ();
			}
		}
	}

#endregion

}