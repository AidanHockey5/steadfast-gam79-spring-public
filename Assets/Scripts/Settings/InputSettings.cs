﻿using UnityEngine;

[System.Serializable]
public class InputSettings
{
    public string MOUSE_X_AXIS = "Mouse X";
    public string MOUSE_Y_AXIS = "Mouse Y";
    public string VERTICAL_AXIS = "Vertical";
    public string HORIZONTAL_AXIS = "Horizontal";
	public string ROTATE_AXIS = "Rotate";
    public string ORBIT_HORIZONTAL_SNAP = "Orbit Horizontal Snap";
    public string ORBIT_HORIZONTAL = "Orbit Horizontal";
    public string ORBIT_VERTICAL = "Orbit Vertical";
    public string ZOOM = "Mouse ScrollWheel";
	public string ATTACK1 = "Fire1";
	public string ATTACK2 = "Fire2";
	public string ATTACK3 = "Fire3";
	public string SPECIAL = "Jump";
}
