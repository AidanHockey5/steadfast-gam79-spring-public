﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingScreen : MonoBehaviour 
{
	public Text loadingText;
	public int iteration = 0;

	void Start()
	{
		StartCoroutine (LoadTimer ());
	}

	IEnumerator LoadTimer()
	{
		yield return new WaitForSeconds (1.0f);

		switch (iteration)
		{
		case 0:
			loadingText.text = "Loading.";
			iteration++;
			break;
		case 1:
			loadingText.text = "Loading..";
			iteration++;
			break;
		case 2:
			loadingText.text = "Loading...";
			iteration++;
			break;
		case 3:
			loadingText.text = "Loading";
			iteration = 0;
			break;
		}

		StartCoroutine (LoadTimer ());
	}
}
