﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Building : MonoBehaviour
{
	public string _objectiveType = string.Empty;
	public bool isDead = false;
	public float deathDelay = 3.0f;
	public BuildingTypes buildingType;
	public Animator buildingAnimator = null;
    public int health = 0;
    public int currentHealth;
    public GameObject rubble = null;
	public int ticketValue;

	private Objective _objective;

	void Awake()
	{
		foreach (BuildingTypes building in BuildingTypes.GetValues(typeof(BuildingTypes))) 
		{
			if (building == buildingType)
			{
				health = (int) building;
                currentHealth = health;
			}
		}
			
		if (GetComponent<Objective> () != null)
		{
			_objective = GetComponent<Objective> ();
			_objectiveType = _objective.objectiveType.ToString();

			//Hard-coding this for audio testing purposes. Please remove and replace with correct code.
			//Sets health if building is an objective:
			//health = 100;
			//currentHealth = health;

        ////Commented this all out since it was the cause of the playerbase dying in one hit - Erith
            if(_objective.objectiveType == ObjectiveTypes.PLAYERBASE) //objectives health was never reassigned.
            {
                health = 10000;
                currentHealth = health;
            }
            else //quick fix for all other objectives
            {
                health = 2000;
                currentHealth = health;
            }
		}
	}

	public void TakeDamage(int damage)
	{
		AudioManager.instance.BldgSoundConfig ("hit", buildingType.ToString (), this.gameObject.transform.position, 1.0f);
		currentHealth -= damage;
        
		print (currentHealth);

		if (this.GetComponent<BuildingFire>() != null)
        {
            this.GetComponent<BuildingFire>().gotHit = false;
        }
        
		if (_objective != null) 
		{
			AudioManager.instance.PlayerFbk (_objectiveType);
		}

		if (_objective != null && currentHealth <= (health * 0.50f))
			AudioManager.instance.PlayerFbk(_objectiveType + "low");

		if (currentHealth <= 0)
        {
            if (_objective != null)
            {
                ObjectiveManager.Instance.CompleteObjective(_objective);
            }

			if (ticketValue > 0)
			{
				GameManager.Instance.BuildingDestroyed (ticketValue);
			}

            DestroyBuilding();
        }

    }

	void Start ()
    {
		buildingAnimator = this.transform.parent.GetComponent<Animator> ();
	}

	public void DestroyBuilding()
	{
		AudioManager.instance.BldgSoundConfig ("collapse", buildingType.ToString (), this.gameObject.transform.position, deathDelay);

		if (_objective != null) 
		{
			AudioManager.instance.PlayerFbk ("objDest");
		}

		Destroy(gameObject.GetComponent<BoxCollider>());
        Rigidbody go = gameObject.GetComponent<Rigidbody> ();

        if (go == null)
        {
            gameObject.AddComponent<Rigidbody>();
        }
       // Instantiate(rubble, this.transform.position, this.transform.rotation);
        Destroy(gameObject, deathDelay);
	}





}
