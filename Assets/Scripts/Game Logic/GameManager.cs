﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;

public class GameManager : NetworkBehaviour
{
	[SerializeField]
	private int _techCost = 1;
	[SerializeField]
	private int _supportCost = 1;
	[SerializeField]
	private int _heavyCost = 3;
	[SerializeField]
	private int _assaultCost = 3;


	[SerializeField]
	private int _ticketAmount;

	[SyncVar (hook = "OnTicketChange")]
	private int _currentTicketAmount = 0;

	private int _numberOfPlayers = 0;
	private int _playersEliminated;

	private bool _death = true;

	private Vector3 _spawnPoint;

	private PlayerControl _playerControl;

	private static GameManager instance_ = null;

	public static GameManager Instance
	{
		get
		{
			if (instance_ != null)
			{
				return instance_;
			}
			else
			{
				GameObject go = new GameObject ();
				return instance_ = go.AddComponent<GameManager> ();
			}
		}
	}

	public  void Awake ()
	{
		instance_ = this;
	}

	void Start ()
	{
		_currentTicketAmount = _ticketAmount;
		UIManager.Instance.UpdateTickets ();
	}

	void Update ()
	{
		//Call player select screen
		if (Input.GetKeyDown (KeyCode.P) && _playerControl != null)
		{
			UIManager.Instance.SetUIState (UIState.CharacterSelect);
		}
      

	}

	public int CurrentTicketAmount()
	{
		return _currentTicketAmount;
	}

	public int CurrentTechCost()
	{
		return _techCost;
	}

	public int CurrentSupportCost()
	{
		return _supportCost;
	}

	public int CurrentHeavyCost()
	{
		return _heavyCost;
	}

	public int CurrentAssaultCost()
	{
		return _assaultCost;
	}

	public void RegisterPlayer (PlayerControl pControl)
	{
		_playerControl = pControl;
	}

	public void SpawnMonster ()
	{
		ChooseSpawnPoint (1);
		_playerControl.SpawnClass (SpawnClass.Monster);
		UIManager.Instance.SetUIState (UIState.Monster);
	}

	public void SpawnSupport ()
	{
		if (CanSpawn (_supportCost))
		{
			if(isServer)
				AudioManager.instance.RpcSound2D ("pepTalk");
			ChooseSpawnPoint (1);
			_playerControl.SpawnClass (SpawnClass.Support);
			UIManager.Instance.SetUIState (UIState.Support);
		}
	}

	public void SpawnTechnician ()
	{
		if (CanSpawn (_techCost))
		{
			if(isServer)
				AudioManager.instance.RpcSound2D ("pepTalk");
			ChooseSpawnPoint (1);
			_playerControl.SpawnClass (SpawnClass.Technician);
			UIManager.Instance.SetUIState (UIState.Technician);
		}
	}

	public void SpawnHeavy ()
	{
		if (CanSpawn (_heavyCost))
		{
			if(isServer)
				AudioManager.instance.RpcSound2D ("mechStart");
			if(isServer)
				AudioManager.instance.RpcSound2D ("pepTalk");
			ChooseSpawnPoint (1);
			_playerControl.SpawnClass (SpawnClass.Heavy);
			UIManager.Instance.SetUIState (UIState.Heavy);
		}
	}

	public void SpawnAssault ()
	{
		if (CanSpawn (_assaultCost))
		{
			if(isServer)
				AudioManager.instance.RpcSound2D ("mechStart");
			if(isServer)
				AudioManager.instance.RpcSound2D ("pepTalk");
			ChooseSpawnPoint (1);
			_playerControl.SpawnClass (SpawnClass.Assault);
			UIManager.Instance.SetUIState (UIState.Assault);
		}
	}

	public bool CanSpawn (int ticketAmountNeeded)
	{
		return _currentTicketAmount - ticketAmountNeeded >= 0;
	}

	public void OnCharacterSpawn (SpawnClass sClass)
	{
		_death = false;
		int cost = 0;
		switch (sClass)
		{
		case SpawnClass.Technician: 
			cost = _techCost;
			break;
		case SpawnClass.Support:
			cost = _supportCost;
			break;
		case SpawnClass.Heavy:
			cost = _heavyCost;
			break;
		case SpawnClass.Assault:
			cost = _assaultCost;
			break;
		}
		UpdateTicketAmount (-(cost));
	}

	public void BuildingDestroyed(int ticketValue)
	{
		CmdLoseTickets (ticketValue);
	}

	[Command]
	public void CmdLoseTickets(int tickets)
	{
        //_currentTicketAmount -= tickets;
        UpdateTicketAmount(-(tickets));  // to route buildings to update ticket ammount 
                                        // so game over cmd is only called once for tickets;
	}

	private void UpdateTicketAmount (int amount)
	{
		_currentTicketAmount += amount;
        Debug.Log("Tickets: " + _currentTicketAmount);
        if (TicketsDepleted()) // edited by Erith
        {
            CmdGameOver(GameOverState.TicketsDepleted); // There was no 
                                                        //lose condition for 0 tickets
        }
    }

	public void OnTicketChange(int amount)
	{
		_currentTicketAmount = amount;
		UIManager.Instance.UpdateTickets ();
	}

	public void MainObjectiveComplete ()
	{
		CmdGameOver (GameOverState.ObjectiveDestroyed);
	}

	public bool TicketsDepleted ()
	{
		return _currentTicketAmount <= 0;
	}

	public bool AllPlayersEliminated ()
	{
		return _playersEliminated == _numberOfPlayers;
	}


	public void OnPlayerDeath ()
	{
		UIManager.Instance.SetUIState (UIState.CharacterSelect);
		_death = true;
	}

	public bool DeathTriggered()
	{
		return _death;
	}

	public void OnMonsterDeath ()
	{
		
	}

	[Command]
	public void CmdGameOver (GameOverState state)
	{
		RpcGameOver (state);
	}

	[ClientRpc]
	public void RpcGameOver(GameOverState state)
	{
		if (isServer)
			AudioManager.instance.RpcSound2D ("gameOver");

		switch (state)
		{
			case GameOverState.ObjectiveDestroyed:
			case GameOverState.TicketsDepleted:
				if (_playerControl == null)
					UIManager.Instance.SetUIState (UIState.MonsterWin);
				else
					UIManager.Instance.SetUIState (UIState.HumansLose);
				break;
			case GameOverState.MonsterDeath:
				if (_playerControl == null)
					UIManager.Instance.SetUIState (UIState.MonsterLose);
				else
					UIManager.Instance.SetUIState (UIState.HumansWin);
				break;

		}
	}

	public void ChooseSpawnPoint (int i)
	{
		_spawnPoint = SpawnPointManager.Instance.SpawnPointLocation (i);
	}

	public Vector3 CurrenSpawnPoint ()
	{
		return _spawnPoint;
	}

	public void AddPlayer ()
	{
		_numberOfPlayers++;
	}

	public void RemovePlayer ()
	{
		_numberOfPlayers--;
		if (_playersEliminated > 0)
			_playersEliminated--;
	}

	public void EndGame()
	{
		CustomNetworkManager.Instance.EndGame ();
	}
}
