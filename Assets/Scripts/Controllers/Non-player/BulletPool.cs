﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class BulletPool : NetworkBehaviour
{

#region Public Fields

	public int poolSize = 30;
	public float lifetime = 1.0f;
	public GameObject bulletPrefab = null;
	public GameObject bulletTrailPrefab = null;

#endregion

#region Private Members

	private GameObject bulletBuffer = null;
	private GameObject bulletTrailBuffer = null;
	private List<GameObject> bullets = new List<GameObject> ();
	private List<GameObject> bulletTrails = new List<GameObject> ();
	private BulletController bulletController = null;
	private BulletTrailController bulletTrailController = null;
	private int iterator = 0;

#endregion

#region Singleton

	private static BulletPool instance = null;

	public static BulletPool Instance
	{
		get
		{
			if (instance != null)
			{
				return instance;
			}
			else
			{
				GameObject go = new GameObject ("Bullet Pool");
				return instance = go.AddComponent<BulletPool> ();
			}
		}
	}

#endregion

#region MonoBehaviours

	private void Awake ()
	{
		instance = this;
	}

	private void Start ()
	{
		if (isServer)
		{
			ClientScene.RegisterPrefab (bulletPrefab);

			for (int i = 0; i < poolSize; i++)
			{
				if (bulletPrefab != null)
				{
					GameObject bullet = (GameObject) Instantiate (bulletPrefab, Vector3.zero, Quaternion.identity);
					bullets.Add (bullet);
					bullet.SetActive (false);
					NetworkServer.Spawn (bullet);
				}
			}
		}
			
		for (int i = 0; i < poolSize; i++)
		{
			if (bulletTrailPrefab != null)
			{
				GameObject bulletTrail = (GameObject) Instantiate (bulletTrailPrefab, Vector3.zero, Quaternion.identity);
				bulletTrails.Add (bulletTrail);
				bulletTrail.SetActive (false);
			}
		}
	}

#endregion

#region Public Functions

	public GameObject RequestBullet (WeaponSettings firingWeapon, Vector3 position, Quaternion rotation)
	{
		bulletBuffer = null;

		for (int i = 0; i < bullets.Count; i++)
		{
			if (!bullets [i].activeInHierarchy)
			{
				bullets [i].SetActive (true);
				bullets [i].transform.position = position;
				bullets [i].transform.rotation = rotation;
				bulletController = bullets [i].GetComponent<BulletController> ();

				if (bulletController != null)
				{
					bulletController.firingWeapon = firingWeapon;
				}
				else
				{
					Debug.LogError ("Bullet prefab missing BulletController component on " + bullets [i].name);
				}

				bulletBuffer = bullets [i];
				break;
			}
		}

		return bulletBuffer;
	}

	public GameObject RequestBulletTrail (WeaponSettings firingWeapon, Vector3 position, Quaternion rotation)
	{
		bulletTrailBuffer = null;

		for (int i = 0; i < bulletTrails.Count; i++)
		{
			if (!bulletTrails [i].activeInHierarchy)
			{
				bulletTrails [i].SetActive (true);
				bulletTrails [i].transform.position = position;
				bulletTrails [i].transform.rotation = rotation;
				bulletTrailController = bulletTrails [i].GetComponent<BulletTrailController> ();

				if (bulletTrailController != null)
				{
					bulletTrailController.firingWeapon = firingWeapon;
				}
				else
				{
					Debug.LogError ("Bullet prefab missing BulletController component on " + bulletTrails [i].name);
				}

				bulletTrailBuffer = bulletTrails [i];
				break;
			}
		}

		return bulletTrailBuffer;
	}

#endregion

}
