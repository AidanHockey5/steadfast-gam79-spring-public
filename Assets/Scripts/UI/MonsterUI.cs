﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MonsterUI : UIController 
{
	[SerializeField]
	private Image _healthBar;
	[SerializeField]
	private Image _fireImage;
	[SerializeField]
	private Image _corrosiveImage;
	[SerializeField]
	private Image _electricImage;
	[SerializeField]
	private Image _sludgeImage;

	private int _health = 0;
	private int _maxHealth = 1;

	private bool _updateCooldowns;

	protected override void OnDataChanged ()
	{
		base.OnDataChanged ();
		if (_monsterData.maxHealth != _maxHealth)
			_maxHealth = _monsterData.maxHealth;
		if (_monsterData.health != _health)
			OnHealthChanged ();		
		
		_fireImage.gameObject.SetActive (_monsterData.hasFire);
		_corrosiveImage.gameObject.SetActive (_monsterData.hasCorrosive);
		_electricImage.gameObject.SetActive (_monsterData.hasElectric);
		_sludgeImage.gameObject.SetActive (_monsterData.hasSludge);
	}

	private void OnHealthChanged()
	{
		_health = _monsterData.health;
		float currentHealth = (float)_health / (float)_maxHealth;
		_healthBar.rectTransform.localScale = new Vector3 (currentHealth, _healthBar.transform.localScale.y, _healthBar.transform.localScale.z);
	}
}
