﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IntroZoomOut : MonoBehaviour
{
    private float speed = 1f;
    private float jumpTimer = 0f;
    private float jumpThres = 6f;
    private float jumpDist = 20f;
    private float doubleJump = 1;

    private int totalJumps = 0;
    
    private float cameraFadeOutDuration = 1f;

    [Header("Script Reference")]
    [SerializeField]
    private FlyThroughCamera ftc = null;
    [SerializeField]
    private BlinkingStart startText = null;



    [Header("Audio")]
    [SerializeField]
    private AudioSource audioSource = null;
    [SerializeField]
    private AudioClip secondSong = null;
    [SerializeField]
    private AudioSource delayedGrowlAudio = null;
    private float growlDelay = 3f;
    private float songTImer = 0f;
    private float songDuration = 500f;

    [Header("UI Elements")]
    [SerializeField]
    private Image title = null;
    private float fadeInPoint = 2f; //Adjusted to show title info much sooner. This is to let the player know that they may skip this sequence at any time.
    private float fadeInTime = 1f;
    [SerializeField]
    private Image topBar = null;
    [SerializeField]
    private Image botBar = null;
    [SerializeField]
    private Image blackOverlay = null;


    void Awake()
    {
        startText.BeginAnim(fadeInPoint + fadeInTime);
    }

    // Use this for initialization
    void Start ()
    {
        if(delayedGrowlAudio != null)
            delayedGrowlAudio.PlayDelayed(growlDelay);

        if(audioSource != null)
            songDuration = audioSource.clip.length;

        ftc = GetComponent<FlyThroughCamera>();

        //Start Faded
        
        title.CrossFadeAlpha(0, 0, true);
        //topBar.CrossFadeAlpha(0, 0, true); //Aspect ratio bars kept at constant alpha
        //botBar.CrossFadeAlpha(0, 0, true);

        topBar.CrossFadeAlpha(1, 2, false);
        botBar.CrossFadeAlpha(1, 2, false);
        
        blackOverlay.CrossFadeAlpha(0, cameraFadeOutDuration, false);
        StartCoroutine(FadeInTitle());
        
    }

    public IEnumerator FadeInTitle()
    {
        yield return new WaitForSeconds(fadeInPoint);

        title.CrossFadeAlpha(1f, fadeInTime, false);
    }
	
	// Update is called once per frame
	void Update ()
    {

        if (totalJumps < 5)
        {
            jumpTimer += Time.deltaTime;
            if (jumpTimer > jumpThres)
            {
                transform.position += transform.forward * -jumpDist * doubleJump;
                doubleJump *= 2.5f;
                jumpTimer = 0;
                totalJumps++;
            }
            else
            {
                transform.position += transform.forward * -speed * Time.deltaTime;
            }
        }

        songTImer += Time.deltaTime;
        if (songTImer > songDuration)
        {
            if(audioSource != null && secondSong != null)
            {
                audioSource.clip = secondSong;
                audioSource.loop = true;
                audioSource.Play();
            }

            if(ftc != null)
                ftc.enabled = true;
            enabled = false;
        }
    }
}
