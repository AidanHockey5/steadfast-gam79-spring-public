﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

	public class BuildingFire : NetworkBehaviour
	{
		ParticleManager pm;
		Building building;
		//ParticleManager particleManager;

		public int particleLimit = 0;

        public bool gotHit = false;

		[SerializeField]
		// private ParticleSystem fireParticle = null;
        private BoxCollider buildingCollider = null;

		private Vector3 minBound;
		private Vector3 maxBound;

		// Use this for initialization
		void Start ()
		{
			pm = ParticleManager.Instance;

			building = GetComponent<Building> ();
			buildingCollider = GetComponent<BoxCollider> ();

			if (GetComponent<BoxCollider> () != null)
			{
				minBound = buildingCollider.bounds.min;
				maxBound = buildingCollider.bounds.max;
			}
		}
	
		// Update is called once per frame
		void Update ()
		{
			if ( this.building.currentHealth < this.building.health && !gotHit)
			{
                
                gotHit = true;
				BuildingOnFire ();//Placed in Monster attack script to acttivate particles needs reference to buildings script
			}
            
		}

		private void SpawnRandomFire ()
		{
			if (pm != null && buildingCollider != null)
			{
				float randomSide = Random.value;
				int minOrMax = Random.Range (0, 2);

				float randomX = Random.Range (minBound.x, maxBound.x);
				float randomY = Random.Range (minBound.y, maxBound.y);
				float randomZ = Random.Range (minBound.z, maxBound.z);

				Vector3 spawnPoint = new Vector3 ();

				if (randomSide > 0.6f)
				{
					if (minOrMax == 0)
						spawnPoint = new Vector3 (maxBound.x, randomY, randomZ);
					else
						spawnPoint = new Vector3 (minBound.x, randomY, randomZ);
				}
				else
				if (randomSide > 0.2)
				{
					if (minOrMax == 0)
						spawnPoint = new Vector3 (randomX, randomY, maxBound.z);
					else
						spawnPoint = new Vector3 (randomX, randomY, minBound.z);
				}
				else
				{
					spawnPoint = new Vector3 (randomX, maxBound.y, randomZ);
				}

				Vector3 direction = transform.position - spawnPoint;
				//Quaternion rotation = Quaternion.FromToRotation(-transform.forward, direction);


				spawnPoint += (direction.normalized * particleLimit);

				// ParticleSystem fireSpawn = (ParticleSystem)Instantiate(fireParticle, spawnPoint, rotation);
               
                    pm.RpcSpawnParticle("Bldg Hit", spawnPoint, 1);
                    pm.RpcSpawnParticle("Dust", spawnPoint, 0);
                    pm.RpcSpawnParticle("FireStart", spawnPoint, 0);
                    pm.RpcSpawnParticle("NEW_Debree_Smoke_Fire", spawnPoint, 0);
                    pm.RpcSpawnParticle("explosion_Small", spawnPoint, 0);
                
			        
				//transform.parent = gameObject.GetComponent<BoxCollider>().transform;
                
			}
		}

		public void BuildingOnFire ()//takes in health from buildings
		{
			SpawnRandomFire ();
			
			if (this.building.rubble != null && this.building.currentHealth <= 50)
			{
                print("rubble");
				pm.RpcSpawnParticle ("NEW_flame&blacksmoke", buildingCollider.transform.position, 10f);
			}
		}
	}

