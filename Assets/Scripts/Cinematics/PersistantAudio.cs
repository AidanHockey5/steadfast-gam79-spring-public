﻿//This script allows the opening sequences and lobby scenes to have smooth audio between scenes with no interuptions
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PersistantAudio : MonoBehaviour 
{
    public static PersistantAudio paInstance = null;
    public static PersistantAudio PAInstance
    {
        get { return paInstance; }
    }


    void Awake() 
    {
        if (paInstance != null && paInstance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else 
            paInstance = this;
     DontDestroyOnLoad(this.gameObject);
     if (SceneManager.GetActiveScene().name != "LobbyScene" && SceneManager.GetActiveScene().name != "CinematicSequence")
         Destroy(gameObject);
 }
}
