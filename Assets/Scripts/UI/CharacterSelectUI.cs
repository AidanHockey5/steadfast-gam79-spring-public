﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterSelectUI : UIController 
{
	[SerializeField]
	private Text _tickets;
	[SerializeField]
	private Text _classOneCost;
	[SerializeField]
	private Text _classTwoCost;
	[SerializeField]
	private Text _classThreeCost;
	[SerializeField]
	private Text _classFourCost;

	void OnEnable()
	{
		UpdateUI ();
	}	

	protected override void OnUpdateTickets ()
	{
		base.OnUpdateTickets ();
		UpdateUI ();
	}

	private void UpdateUI()
	{
		_tickets.text = "Tickets Remaining: " + GameManager.Instance.CurrentTicketAmount ().ToString();
		_classOneCost.text = GameManager.Instance.CurrentTechCost ().ToString() + " Ticket(s)";
		_classTwoCost.text = GameManager.Instance.CurrentSupportCost ().ToString() + " Ticket(s)";
		_classThreeCost.text = GameManager.Instance.CurrentHeavyCost ().ToString() + " Ticket(s)";
		_classFourCost.text = GameManager.Instance.CurrentAssaultCost ().ToString() + " Ticket(s)";
	}
}
