﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AttackAreaController : MonoBehaviour
{

#region Public

	[NonSerialized]
	public MonsterAbilitySettings monsterAbilitySettings = null;
	[NonSerialized]
	public List<GameObject> hitBuildings, hitPlayers = null;
	public bool isDamaging = false;

#endregion

#region Private

	private float timeCounter = 0;

#endregion

#region MonoBehaviours

	private void Start ()
	{
		hitBuildings = new List<GameObject> ();
		hitPlayers = new List<GameObject> ();
		gameObject.SetActive (false);
	}

	private void OnTriggerEnter (Collider col)
	{
		if (monsterAbilitySettings != null)
		{
			AddTarget (col.gameObject);
		}
	}

	private void OnTriggerStay (Collider col)
	{
		if (monsterAbilitySettings != null && isDamaging)
		{
			DamageOverTime ();
		}
	}

	private void OnTriggerExit (Collider col)
	{
		if (monsterAbilitySettings != null)
		{
			RemoveTarget (col.gameObject);
		}
	}

	private void OnEnable ()
	{
		timeCounter = 0;
	}

	private void OnDisable ()
	{
		isDamaging = false;
	}

#endregion

#region Damage

	public void InstantDamage ()
	{
		foreach (var building in hitBuildings)
		{
			if (building != null)
			{
				building.GetComponent<Building> ().TakeDamage (monsterAbilitySettings.damage);
			}
		}

		hitBuildings.Clear ();

		foreach (var player in hitPlayers)
		{
			if (player != null)
			{
				player.GetComponent<PlayerObject> ().RequestTakeDamage (GameEvent.HIT_FROM_MONSTER, monsterAbilitySettings.damage);

			}
		}

		hitPlayers.Clear ();
	}

	private void DamageOverTime ()
	{
		timeCounter += Time.deltaTime;

		if (timeCounter >= monsterAbilitySettings.tickRate)
		{
			foreach (var building in hitBuildings)
			{
				if (building != null)
				{
					building.GetComponent<Building> ().TakeDamage (monsterAbilitySettings.damage);
				}
			}

			foreach (var player in hitPlayers)
			{
				if (player != null)
				{
					player.GetComponent<PlayerObject> ().RequestTakeDamage (GameEvent.HIT_FROM_MONSTER, monsterAbilitySettings.damage);
				}
			}

			timeCounter = 0;
		}
	}

	private void AddTarget (GameObject hitTarget)
	{
		Building hitBuilding = hitTarget.GetComponent<Building> ();
		PlayerObject hitPlayer = hitTarget.GetComponent<PlayerObject> ();

		if (hitBuilding != null)
		{
			hitBuildings.Add (hitTarget);
		}

		if (hitPlayer != null)
		{
			hitPlayers.Add (hitTarget);
		}
	}

	private void RemoveTarget (GameObject hitTarget)
	{
		Building hitBuilding = hitTarget.GetComponent<Building> ();
		PlayerObject hitPlayer = hitTarget.GetComponent<PlayerObject> ();

		if (hitBuilding != null)
		{
			hitBuildings.Remove (hitTarget);
		}

		if (hitPlayer != null)
		{
			hitPlayers.Remove (hitTarget);
		}
	}

#endregion

}
