﻿using UnityEngine;

[System.Serializable]
public class MonsterAbilitySettings
{
    public string name = "Ability";
    public int damage = 100;
    public bool isDamageOverTime = false;
    public float tickRate = 1.0f;
    public float physicsForce = 1000.0f;
}