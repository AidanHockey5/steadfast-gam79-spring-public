﻿using UnityEngine;
using System.Collections;


public class SpawnPoints : MonoBehaviour 
{
    public int pointNumber;

	void Start () 
    {
		SpawnPointManager.Instance.RegisterSpawnPoint (pointNumber, this);
	}
}
