﻿/*using UnityEngine;
using System.Collections;

public class BuildingFire : MonoBehaviour
{
	[SerializeField]
	private ParticleSystem fireParticle = null;

	[SerializeField]
	private GameObject destructibleBuilding = null;

	private BoxCollider buildingCollider = null;

	private Vector3 minBound;
	private Vector3 maxBound;

	private int damagedCount = 0;
	private bool swapTriggered = false;

	// Use this for initialization
	void Start ()
	{
		buildingCollider = GetComponent<BoxCollider> ();

		if (buildingCollider != null)
		{
			minBound = buildingCollider.bounds.min;
			maxBound = buildingCollider.bounds.max;
		}
	}

	private void SpawnRandomFire ()
	{
		if (fireParticle != null && buildingCollider != null)
		{
			float randomSide = Random.value;
			int minOrMax = Random.Range (0, 2);

			float randomX = Random.Range (minBound.x, maxBound.x);
			float randomY = Random.Range (minBound.y, maxBound.y);
			float randomZ = Random.Range (minBound.z, maxBound.z);

			Vector3 spawnPoint = new Vector3 ();

			if (randomSide > 0.6f)
			{
				if (minOrMax == 0)
					spawnPoint = new Vector3 (maxBound.x, randomY, randomZ);
				else
					spawnPoint = new Vector3 (minBound.x, randomY, randomZ);
			}
			else
			if (randomSide > 0.2)
			{
				if (minOrMax == 0)
					spawnPoint = new Vector3 (randomX, randomY, maxBound.z);
				else
					spawnPoint = new Vector3 (randomX, randomY, minBound.z);
			}
			else
			{
				spawnPoint = new Vector3 (randomX, maxBound.y, randomZ);
			}

			Vector3 direction = transform.position - spawnPoint;
			Quaternion rotation = Quaternion.FromToRotation (-transform.forward, direction);

			spawnPoint += (direction.normalized * 5);

			ParticleSystem fireSpawn = (ParticleSystem) Instantiate (fireParticle, spawnPoint, rotation);

            

			fireSpawn.transform.parent = transform;

			damagedCount++;
			if (damagedCount > 5 && swapTriggered == false)
			{
				StartCoroutine (SwapModels ());
				swapTriggered = true;
			}
		}
	}

	private IEnumerator SwapModels ()
	{
		yield return new WaitForSeconds (0.5f);
		Destroy (gameObject);
		if (destructibleBuilding != null)
			Instantiate (destructibleBuilding, transform.position, transform.rotation);
	}
}*/
