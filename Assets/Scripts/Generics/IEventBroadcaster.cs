﻿using System;

public interface IEventBroadcaster
{
	event EventHandler<GameEventArgs> EventHandlerMainDelegate;

	void RegisterHandler (EventHandler<GameEventArgs> EventHandlerDelegate);

	void UnRegisterHandler (EventHandler<GameEventArgs> EventHandlerDelegate);

	void BroadcastEvent (GameEvent type, float amount);

	void BroadcastEvent (GameEvent type, bool condition);

	void BroadcastEvent (GameEvent type, params object[] args);
}
