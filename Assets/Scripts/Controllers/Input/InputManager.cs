﻿using UnityEngine;
using System;
using System.Collections;

public class InputManager : MonoBehaviour, IEventBroadcaster
{

#region Public Fields

	public event EventHandler<GameEventArgs> EventHandlerMainDelegate;

	public InputSettings input = new InputSettings ();
	public bool isBroadcasting = true;

#endregion

#region Private

	private bool attack1, attack2, attack3, special = false;
	private float horizontal, vertical, mouseHorizontal, mouseVertical, rotate = 0.0f;
	private GameEventArgs buffer = new GameEventArgs ();

#endregion

#region Singleton

	private static InputManager instance = null;

	public static InputManager Instance
	{
		get
		{
			if (instance != null)
			{
				return instance;
			}
			else
			{
				GameObject go = new GameObject ();
				return instance = go.AddComponent<InputManager> ();
			}
		}
	}

#endregion

#region MonoBehaviours

	private void Awake ()
	{
		instance = this;
	}

	private void Start ()
	{
		isBroadcasting = true;
	}

	private void OnDestroy ()
	{
		EventHandlerMainDelegate = null;
	}

	private void Update ()
	{
		if (!UIManager.Instance.IsPlayable ())
			return;

		if (isBroadcasting)
		{
			GetInput ();
		}
	}

	private void LateUpdate ()
	{
		if (!UIManager.Instance.IsPlayable ())
			return;

		if (isBroadcasting)
		{
			GetInput_Late ();
		}
	}

	private void FixedUpdate ()
	{
		if (!UIManager.Instance.IsPlayable ())
			return;
		
		if (isBroadcasting)
		{
			GetInput_Fixed ();
		}
	}

#endregion

#region GetInput Functions

	private void GetInput ()
	{
		attack1 = Input.GetButton (input.ATTACK1);
		attack2 = Input.GetButton (input.ATTACK2);
		attack3 = Input.GetButton (input.ATTACK3);


		if (attack1)
			BroadcastEvent (GameEvent.CHARACTER_FIRE1, attack1);
		if (attack2)
			BroadcastEvent (GameEvent.CHARACTER_FIRE2, attack2);
		if (attack3)
			BroadcastEvent (GameEvent.CHARACTER_FIRE3, attack3);
	}

	private void GetInput_Fixed ()
	{
		horizontal = 0.0f;
		vertical = 0.0f;

		horizontal = Input.GetAxis (input.HORIZONTAL_AXIS);
		vertical = Input.GetAxis (input.VERTICAL_AXIS);
		special = Input.GetButton (input.SPECIAL);

		BroadcastEvent (GameEvent.HORIZONTAL_INPUT, horizontal);
		BroadcastEvent (GameEvent.VERTICAL_INPUT, vertical);
		if (special)
			BroadcastEvent (GameEvent.CHARACTER_JUMP, special);
	}

	private void GetInput_Late ()
	{
		mouseHorizontal = Input.GetAxis (input.MOUSE_X_AXIS);
		mouseVertical = Input.GetAxis (input.MOUSE_Y_AXIS);
		rotate = Input.GetAxis (input.ROTATE_AXIS);

		if (mouseHorizontal != 0)
			BroadcastEvent (GameEvent.MOUSE_X_INPUT, mouseHorizontal);
		if (mouseVertical != 0)
			BroadcastEvent (GameEvent.MOUSE_Y_INPUT, mouseVertical);
		if (rotate != 0)
			BroadcastEvent (GameEvent.CHARACTER_ROTATE, rotate);
	}

#endregion

#region IEventBroadcaster

	public void RegisterHandler (EventHandler<GameEventArgs> EventHandlerDelegate)
	{
		EventHandlerMainDelegate += EventHandlerDelegate;
	}

	public void UnRegisterHandler (EventHandler<GameEventArgs> EventHandlerDelegate)
	{
		EventHandlerMainDelegate -= EventHandlerDelegate;
	}

	public void BroadcastEvent (GameEvent eventType, float amount)
	{
		buffer.eventType = eventType;
		buffer.amount = amount;

		if (EventHandlerMainDelegate != null)
		{
			EventHandlerMainDelegate (this, buffer);
		}
	}

	public void BroadcastEvent (GameEvent eventType, bool condition)
	{
		buffer.eventType = eventType;
		buffer.condition = condition;

		if (EventHandlerMainDelegate != null)
		{
			EventHandlerMainDelegate (this, buffer);
		}
	}

	public void BroadcastEvent (GameEvent eventType, params object[] args)
	{
		buffer.eventType = eventType;
		buffer.eventArgs = args;

		if (EventHandlerMainDelegate != null)
		{
			EventHandlerMainDelegate (this, buffer);
		}
	}

#endregion

}
