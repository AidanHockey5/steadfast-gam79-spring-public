﻿using UnityEngine;
using System.Collections;

public class DestroyCube : MonoBehaviour 
{
    public bool canDestroy = false;

    void OnTriggerEnter(Collider other) 
    {
        if (canDestroy && other.tag == "Destroyable")
        {
            Destroy(other.gameObject);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (canDestroy && other.tag == "Destroyable")
        {
            Destroy(other.gameObject);
        }
    }
}
