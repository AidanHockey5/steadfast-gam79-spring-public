﻿using UnityEngine;

[System.Serializable]
public class CameraSettings
{
	public float lookSensitivity = 10.0f;
	public float xLowerCameraBound = -90;
	public float xUpperCameraBound = 90;

    public CameraSettings()
	{
		
	}
}
