﻿using UnityEngine;

[System.Serializable]
public class AnimatorSettings
{
	public CharacterState currentState = CharacterState.IDLE;
	public string RUN_Y_FLOAT = "Run Y";
	public string RUN_X_FLOAT = "Run X";
	public string TURN_FLOAT = "Turn";
	public string MOUSE_X = "Mouse X";
	public string MOUSE_Y = "Mouse Y";
	public string ISMOVING_BOOL = "isMoving";
	public string FIRE_1_BOOL = "isAttacking";
	public string FIRE_2_BOOL = "isGroundPound";
	public string FIRE_3_BOOL = "isBreathAttack";
	public string FIRE_1_TRIGGER = "Attack";
	public string FIRE_2_TRIGGER = "Ground Pound";
	public string FIRE_3_TRIGGER = "Breath Attack";
	public string SPECIAL_MOVE = "Special Move";
	public string WAS_HIT = "Hit";
}
