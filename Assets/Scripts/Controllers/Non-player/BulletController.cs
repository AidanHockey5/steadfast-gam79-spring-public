﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;

public class BulletController : NetworkBehaviour
{

#region Public

	[NonSerialized] public WeaponSettings firingWeapon;
    ParticleManager pm;
#endregion

#region Private

	private BulletSettings bullet = new BulletSettings ();
	private Vector3 posLastFrame;
	private RaycastHit rayHit;

#endregion

#region MonoBehaviours

	private void Start ()
	{
		posLastFrame = transform.position;
        pm = ParticleManager.Instance;
	}

	private void Update ()
	{
		Move ();
		CheckCollision ();
	}

#endregion

#region Functions

	private void Move ()
	{
		posLastFrame = transform.position;
		transform.position += transform.forward * bullet.speed * Time.deltaTime;
	}

	private void CheckCollision ()
	{
		if (!isServer)
			return;

		if (Physics.Linecast (posLastFrame, transform.position, out rayHit))
		{
			SendHit (rayHit.collider.gameObject);
		}
	}

	private void SendHit (GameObject hit)
	{
		PlayerObject playerObj = hit.GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			playerObj.RequestTakeDamage (GameEvent.HIT_FROM_HUMAN, firingWeapon.power);
            pm.RpcSpawnParticle("BulletSpark", transform.position, 1f);
			if (gameObject.activeInHierarchy)
			{
				gameObject.SetActive(false);
			}
		}
	}

#endregion

}
