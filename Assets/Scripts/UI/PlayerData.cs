﻿using UnityEngine;
using System.Collections;

public class PlayerData 
{
	public int maxHealth;
	public int health;
	public int maxShield;
	public int shield;
	public int maxClip;
	public int clip;
	public float abilityOneCooldown;
	public float abilityTwoCooldown;
	public float reloadTime;
}
