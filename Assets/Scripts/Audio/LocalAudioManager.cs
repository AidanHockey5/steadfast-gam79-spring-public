﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LocalAudioManager : MonoBehaviour 
{
	public static LocalAudioManager instance = null;

	public List<AudioClip> clips;
	private Dictionary<string, AudioClip> pickedAudio;

	public List<AudioMixerGroup> mixerGroups;
	private Dictionary<string, AudioMixerGroup> pickedMixerGroup;

	public List<AudioMixerSnapshot> mixerSnapshots;
	private Dictionary<string, AudioMixerSnapshot> pickedSnapshot;

	public GameObject soundObjectPrefab = null;
	private GameObject soundObject = null;
	private AudioSource source = null;

	private GameObject musicOne = null;
	private GameObject musicTwo = null;

	public float delay = 31.599f;

	void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
		DontDestroyOnLoad(gameObject);
	}

	void Start ()
	{
		pickedAudio = new Dictionary<string, AudioClip>();
		foreach (AudioClip a in clips)
			pickedAudio.Add(a.name, a);

		pickedMixerGroup = new Dictionary<string, AudioMixerGroup>();
		foreach (AudioMixerGroup m in mixerGroups)
			pickedMixerGroup.Add(m.name, m);

		pickedSnapshot = new Dictionary<string, AudioMixerSnapshot>();
		foreach (AudioMixerSnapshot s in mixerSnapshots)
			pickedSnapshot.Add(s.name, s);

		pickedSnapshot ["DefaultMain"].TransitionTo (0.0f);
		pickedSnapshot ["DefaultMenuMusic"].TransitionTo (0.0f);

		LocalSound("menuMusic");
	}

	public void LocalSound (string audioEvent)
	{
		if (audioEvent != "musicFade") 
		{
			soundObject = (GameObject)Instantiate (soundObjectPrefab, transform.position, transform.rotation);
			source = soundObject.GetComponent<AudioSource> ();
			source.spatialBlend = 0.0f;
		}

		if (audioEvent == "menuMusic") 
		{
			pickedSnapshot ["DefaultMenuMusic"].TransitionTo (0.0f);
			pickedSnapshot ["DefaultMain"].TransitionTo (0.0f);
			source.loop = true;
			musicOne = soundObject;
			DontDestroyOnLoad(musicOne);
			Play(soundObject, source, "menuMusic", "MainMenu");
		}

        if (audioEvent == "menuMusicLoopVo")
        {
            pickedSnapshot["DefaultMain"].TransitionTo(0.0f);
            Play(soundObject, source, "menuMusicLoopVo", "MX");
            soundObject.AddComponent<VolumeFade>();
        }

		if (audioEvent == "gameStart") 
		{
			pickedSnapshot ["CutsceneMusicFade"].TransitionTo (1.0f);

			/*if (musicOne != null)
				Destroy (musicOne, 1.0f);
			if (musicTwo != null)
				Destroy (musicTwo, 1.0f);*/

			Destroy (gameObject, 2.0f);
		}

        if (audioEvent == "monIntro")
            Play(soundObject, source, "voIntroMon", "VX");

        if (audioEvent == "defIntro")
            Play(soundObject, source, "voIntroDef", "VX");

		if (audioEvent == "selectMon") 
		{
			pickedSnapshot ["DefaultMusicFade"].TransitionTo (0.5f);
		}

		if (audioEvent == "selectHum") 
		{
			pickedSnapshot ["DefaultMusicFade"].TransitionTo (0.5f);
		}

        /*if (audioEvent == "click")
            Play(soundObject, source, "uiClick", "System");

        if (audioEvent == "select")
            Play(soundObject, source, "uiSelect", "System");

        if (audioEvent == "slider")
            Play(soundObject, source, "uiSlider", "System");*/

		/*if (audioEvent == "gameStart") 
		{
			if (musicOne != null)
				Destroy (musicOne);
			
			Destroy (gameObject);
		}*/
	}

	/*IEnumerator MusicDelay (GameObject soundObject, AudioSource source)
	{
			musicOne = soundObject;
			DontDestroyOnLoad(musicOne);
			Play(musicOne, source, "menuMusicIntro", "MainMenu");
            musicTwo = (GameObject)Instantiate(soundObjectPrefab, transform.position, transform.rotation);
			DontDestroyOnLoad(musicTwo);
            AudioSource sourceTwo = musicTwo.GetComponent<AudioSource>();
            sourceTwo.spatialBlend = 0.0f;
            sourceTwo.loop = true;
            yield return new WaitForSeconds(delay);
            Play(musicTwo, sourceTwo, "menuMusicLoop", "MainMenu");
	}*/

	//Plays the sound
	public void Play(GameObject soundObject, AudioSource source, string sound, string mixer)
	{
		source.clip = pickedAudio[sound];
		source.outputAudioMixerGroup = pickedMixerGroup[mixer];
		source.Play ();

		if (!source.loop)
			Destroy (soundObject, source.clip.length);
	}
}