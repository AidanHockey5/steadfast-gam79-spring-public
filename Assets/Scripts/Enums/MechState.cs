﻿public enum MechState
{
	NONE,
	IDLE,
	MOVING,
	ATTACKING,
	JUMPING,
	DEAD
}

