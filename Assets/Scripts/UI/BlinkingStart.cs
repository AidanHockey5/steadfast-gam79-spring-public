﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BlinkingStart : MonoBehaviour
{
    private Text startText = null;
    private float pressStartFadeTime = 1f;
    private bool startFadeIn = true;

    void Awake()
    {
        startText = GetComponent<Text>();
    }
    
    public void BeginAnim(float delay)
    {
        startText.CrossFadeAlpha(0, 0, true);
        StartCoroutine(PressStart(delay));
	}

    public IEnumerator PressStart(float delay)
    {
        yield return new WaitForSeconds(delay);

        if (startFadeIn)
        {
            startText.CrossFadeAlpha(1f, pressStartFadeTime, false);
        }
        else
        {
            startText.CrossFadeAlpha(0f, pressStartFadeTime, false);
        }
        startFadeIn = !startFadeIn;
        StartCoroutine(PressStart(pressStartFadeTime));
    }

}
