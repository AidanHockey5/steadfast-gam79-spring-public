﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;

[RequireComponent (typeof(PlayerObject))]
public class WeaponController : NetworkBehaviour, IEventListener
{

#region Public

	[Header ("SyncVars")]
	public bool isReloading = false;
	public bool canFire = true;
	public int currentAmmo = 0;

#endregion

#region Private

	[SerializeField]
	private Transform bulletSpawn;
	[SerializeField]
	private Transform trailSpawn;

	private WeaponSettings[] equippableWeapons = null;
	private WeaponSettings currentWeapon = null;

	private PlayerObject playerObject = null;

#endregion

#region MonoBehaviours

	private void Start ()
	{
		if(isLocalPlayer)
			Subscribe ();

		playerObject = GetComponent<PlayerObject> ();

		if (playerObject != null)
		{
			equippableWeapons = playerObject.equippableWeapons;
		}
		SetWeapon (0);
	}

	private void OnDestroy ()
	{
		UnSubscribe ();
	}

#endregion

#region Functions

	private void SetWeapon (int slot)
	{
		//canFire = false;
		currentWeapon = equippableWeapons [slot];
		currentAmmo = currentWeapon.maxAmmo;
		//StartCoroutine (ReloadAfterTime (currentWeapon.reloadTime));
	}

	private IEnumerator FireAfterTime (float seconds)
	{
		yield return new WaitForSeconds (seconds);
		canFire = true;
	}

	private IEnumerator ReloadAfterTime (float seconds)
	{
		yield return new WaitForSeconds (seconds);
		Reload ();
		canFire = true;
		isReloading = false;
	}

	private IEnumerator DeactivateBulletAfterTime (GameObject bullet, float seconds)
	{
		yield return new WaitForSeconds (seconds);

		if (bullet.activeInHierarchy)
		{
			bullet.SetActive (false);
			bullet.transform.position = Vector3.zero;
			bullet.transform.rotation = Quaternion.identity;
		}
	}

#endregion

#region NetCode

	[Command]
	private void CmdFire (Vector3 bulletSpawnPosition, Quaternion bulletSpawnRotation)
	{		
		if (currentWeapon.name == "Gun") 
			AudioManager.instance.RpcSound ("gunshot", gameObject);

		GameObject lastBulletFired = BulletPool.Instance.RequestBullet (currentWeapon, bulletSpawnPosition, bulletSpawnRotation);

		if (lastBulletFired == null)
		{
			Debug.LogError ("Bullet pool could not find unused bullet, consider increasing pool size.");
		}
		else
		{
			StartCoroutine (DeactivateBulletAfterTime (lastBulletFired, currentWeapon.bulletSettings.lifetime));
		}
	}

	private void Reload ()
	{
		if (currentAmmo < currentWeapon.maxAmmo)
		{
			currentAmmo = currentWeapon.maxAmmo;
			playerObject.Reload ();
		}
	}

	[Client]
	private void ShowBulletTrail (Vector3 position, Quaternion rotation)
	{
		GameObject bulletTrail = BulletPool.Instance.RequestBulletTrail (currentWeapon, position, rotation);

		if (bulletTrail == null)
		{
			Debug.LogError ("Bullet pool could not find unused bullet trail, consider increasing pool size.");
		}
		else
		{
			StartCoroutine (DeactivateBulletAfterTime (bulletTrail, currentWeapon.bulletSettings.lifetime));
		}
	}

#endregion

#region IEventListener

	public void Subscribe ()
	{
		InputManager.Instance.RegisterHandler (ReceiveBroadcast);
	}

	public void UnSubscribe ()
	{
		InputManager.Instance.UnRegisterHandler (ReceiveBroadcast);
		PlayerObject playerObj = GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			equippableWeapons = null;
		}
	}

	public void ReceiveBroadcast (object sender, GameEventArgs gameEventArgs)
	{
		if (!UIManager.Instance.IsPlayable ())
		{
			return;
		}

		switch (gameEventArgs.eventType)
		{
			case GameEvent.CHARACTER_FIRE1:
				{
					if (bulletSpawn != null || trailSpawn != null)
					{
						if (gameEventArgs.condition)
						{
							if (!isReloading && canFire)
							{
								canFire = false;
								currentAmmo--;
								playerObject.UseAmmo ();
								CmdFire (bulletSpawn.position, bulletSpawn.rotation);								
								ShowBulletTrail (trailSpawn.position, bulletSpawn.rotation);
								if (currentAmmo <= 0)
								{
									isReloading = true;
									UIManager.Instance.TriggerCooldown (3);
									StartCoroutine (ReloadAfterTime (currentWeapon.reloadTime));
								}
								else
								{
									StartCoroutine (FireAfterTime (currentWeapon.attackRate));
								}
							}
						}
					}
					else
					{
						if (bulletSpawn == null)
						{
							Debug.LogError ("Bullet spawn transform not assigned on " + gameObject.name);
						}

						if (trailSpawn == null)
						{
							Debug.LogError ("Trail spawn transform not assigned on " + gameObject.name);
						}
					}
				}
				break;
			case GameEvent.CHARACTER_RELOAD:
				{
					// index 0 - reloadKeyPressed
					if (gameEventArgs.condition)
					{
						Reload ();
					}
				}
				break;
		}
	}

#endregion

}
