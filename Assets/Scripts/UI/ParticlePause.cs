﻿using UnityEngine;
using System.Collections;

public class ParticlePause : MonoBehaviour
{
    private ParticleSystem p = null;

	// Use this for initialization
	void Start ()
    {
        p = GetComponent<ParticleSystem>();
        p.Pause(true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
