﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[RequireComponent (typeof(Animator))]
[RequireComponent (typeof(NetworkAnimator))]
public class AnimatorCharacterController : NetworkBehaviour
{

#region Public Fields

#endregion

#region Private Members

	private AnimatorSettings animatorSettings = null;
	private Animator animator = null;
	private NetworkAnimator networkAnimator = null;

#endregion

#region MonoBehaviours

	private void Start ()
	{
		animator = GetComponent<Animator> ();
		networkAnimator = GetComponent<NetworkAnimator> ();

		if (isLocalPlayer)
		{
			Subscribe ();
		}
		else
		{
			animator.applyRootMotion = false;
		}
	}

	private void OnDestroy ()
	{
		UnSubscribe ();
	}

#endregion

#region Net Code

	//[Command]
	private void CmdSetBool (string name, bool onOff)
	{
		animator.SetBool (name, onOff);
	}

	//[Command]
	private void CmdSetFloat (string name, float value)
	{
		animator.SetFloat (name, value);
	}

	//[Command]
	private void CmdSetTrigger (string name)
	{
		networkAnimator.SetTrigger (name);
	}

#endregion

#region IEventListener

	public void Subscribe ()
	{
		InputManager.Instance.RegisterHandler (ReceiveBroadcast);
		PlayerObject playerObj = GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			animatorSettings = playerObj.animatorSettings;
		}
	}

	public void UnSubscribe ()
	{
		InputManager.Instance.UnRegisterHandler (ReceiveBroadcast);
		PlayerObject playerObj = GetComponent<PlayerObject> ();

		if (playerObj != null)
		{
			animatorSettings = null;
		}
	}

	public void ReceiveBroadcast (object sender, GameEventArgs gameEventArgs)
	{
		switch (gameEventArgs.eventType)
		{
			case GameEvent.HORIZONTAL_INPUT:
				{
					CmdSetFloat (animatorSettings.RUN_X_FLOAT, gameEventArgs.amount);
				}
				break;
			case GameEvent.VERTICAL_INPUT:
				{
					CmdSetFloat (animatorSettings.RUN_Y_FLOAT, gameEventArgs.amount);
				}
				break;
			case GameEvent.CHARACTER_ROTATE:
				{
					CmdSetFloat (animatorSettings.TURN_FLOAT, gameEventArgs.amount);
				}
				break;
			case GameEvent.MOUSE_X_INPUT:
				{
					CmdSetFloat (animatorSettings.MOUSE_X, gameEventArgs.amount);
				}
				break;
			case GameEvent.MOUSE_Y_INPUT:
				{
					CmdSetFloat (animatorSettings.MOUSE_Y, gameEventArgs.amount);
				}
				break;
			case GameEvent.CHARACTER_JUMP:
				{
					CmdSetBool (animatorSettings.SPECIAL_MOVE, gameEventArgs.condition);
					CmdSetTrigger (animatorSettings.SPECIAL_MOVE);
				}
				break;
			case GameEvent.CHARACTER_FIRE1:
				{				
					CmdSetBool (animatorSettings.FIRE_1_BOOL, gameEventArgs.condition);
					CmdSetTrigger (animatorSettings.FIRE_1_TRIGGER);
				}
				break;
			case GameEvent.CHARACTER_FIRE2:
				{
					CmdSetBool (animatorSettings.FIRE_2_BOOL, gameEventArgs.condition);
					CmdSetTrigger (animatorSettings.FIRE_2_TRIGGER);
				}
				break;
			case GameEvent.CHARACTER_FIRE3:
				{
					CmdSetBool (animatorSettings.FIRE_3_BOOL, gameEventArgs.condition);
					CmdSetTrigger (animatorSettings.FIRE_3_TRIGGER);
				}
				break;
		}
	}

	public void EndSpecialMove()
	{
		CmdSetBool ("JetPackTakeOff", false);
		CmdSetTrigger ("JetPackLand");
	}

#endregion

}
