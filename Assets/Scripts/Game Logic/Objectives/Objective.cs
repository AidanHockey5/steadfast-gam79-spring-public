﻿using UnityEngine;
using System.Collections;

public class Objective : MonoBehaviour
{
	public ObjectiveTypes objectiveType;
	public string title = "Objective Title";
	public string description = "Objective Description";
	public int objectivePriority = 0;
	public bool isMainObjective;
	public Vector3 location = Vector3.zero;

	private bool _complete;

	protected virtual void Start()
	{
		location = gameObject.transform.position;
		ObjectiveManager.Instance.RegisterObjectve (this);
	}
}
