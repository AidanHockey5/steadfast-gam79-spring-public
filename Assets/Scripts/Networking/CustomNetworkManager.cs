﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Net;
using System.Net.Sockets;

public class CustomNetworkManager : NetworkManager 
{
	public NetworkDiscovery discovery;

	private static CustomNetworkManager _instance;
	private static bool _isHost = false;
	private bool isHuman;
	private int _hostID;

	public static CustomNetworkManager Instance
	{
		get { return _instance; }
		private set { _instance = value; }
	}

	public bool IsHost
	{
		get { return _isHost; }
		private set { _isHost = value; }
	}

	public bool IsHuman
	{
		get { return isHuman; }
		private set { isHuman = value; }
	}

	bool _initialized;
	public bool Initialized 
	{ 
		get { return _initialized; }
		set { _initialized = value; }
	}

	void Start()
	{
		Instance = this;
		Initialized = false;
		if(discovery == null)
			discovery = GameObject.Find ("NetworkDiscovery").GetComponent<NetworkDiscovery> ();
	}

	void OnEnable()
	{
		if(discovery == null)
			discovery = GameObject.Find ("NetworkDiscovery").GetComponent<NetworkDiscovery> ();
	}

	public override void OnStartHost ()
	{
		base.OnStartHost ();
		if(discovery == null)
			discovery = GameObject.Find ("NetworkDiscovery").GetComponent<NetworkDiscovery> ();
		IsHost = true;
		StartBroadcasting ();
	}


	public void SetMonser()
	{
		if(discovery == null)
			discovery = GameObject.Find ("NetworkDiscovery").GetComponent<NetworkDiscovery> ();
		IsHost = true;
		networkAddress = LocalIP ();
		playerPrefab = spawnPrefabs[1];
		if (IsHost)
			StartHost ();
		else
			StartClient ();
	}

	public void SetSupport()
	{
		if(discovery == null)
			discovery = GameObject.Find ("NetworkDiscovery").GetComponent<NetworkDiscovery> ();
		playerPrefab = spawnPrefabs[0];
		if (IsHost)
		{
			isHuman = true;
			StartHost ();
		}
		else
		{
			StartClient ();
		}
	}

	public void JoinMatch(string matchAddress)
	{
		MainMenu.Instance.GoToLoadScreen ();
		networkAddress = matchAddress;
		StartClient ();
	}

	public override void OnClientSceneChanged (NetworkConnection conn)
	{
		base.OnClientSceneChanged (conn);

		if (!IsHost)
		{			
			UIManager.Instance.SetUIState (UIState.CharacterSelect);
		}
		else
		{			
			GameManager.Instance.AddPlayer ();
			if (isHuman)
				UIManager.Instance.SetUIState (UIState.CharacterSelect);
			else
				UIManager.Instance.SetUIState (UIState.Monster);
		}
	}

	public void StartBroadcasting()
	{
		discovery.Initialize();
		discovery.StartAsServer ();
	}

	public void StartListening()
	{
		discovery.Initialize();
		discovery.StartAsClient ();
	}

	public void StopBroadcasting()
	{
		if (discovery.isClient || discovery.isServer)
			return;

		discovery.StopBroadcast ();
	}

	public Dictionary<string, NetworkBroadcastResult> GetHostList()
	{
		return discovery.broadcastsReceived;
	}

	public void StartHosting()
	{
		IsHost = true;
		networkAddress = LocalIP ();
		//StartHost ();
	}

	public string LocalIP()
	{
		IPHostEntry host;
		string localIP = "";
		host = Dns.GetHostEntry(Dns.GetHostName());
		foreach (IPAddress ip in host.AddressList)
		{
			if (ip.AddressFamily == AddressFamily.InterNetwork)
			{
				localIP = ip.ToString();
				break;
			}
		}
		return localIP;
	}

	public void EndGame()
	{
		StopClient ();
		if(IsHost)
			StopHost ();
	}
}
